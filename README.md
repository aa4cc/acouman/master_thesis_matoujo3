# Master_thesis_template_latex

# Table of Content (ToC):
To get correct table of content, run (in Texmaker) "PdfLaTex + View PDF" twice.

# Missing figures
There are some figures in SVG format that need to be converted to PDF.
There is an automated script to do this.
You need to have Inkscape installed to run it.

On Windows, run `convert_figures.bat`.
On Linux, run `convert_figures.sh`.

# Nomenclature:
To get nomenclature, you need to run command:
- "makeindex %.nlo -s nomencl.ist -o %.nls -t %.nlg"

This command can be added (in Texmaker) as user defined command. Rebuild "PdfLaTex + View PDF" to add the nomenclature in the ToC.

- Beware: (maybe stupid note but I did this mistake) always edit the source file "nomenclature.tex" not the temporary file "thesis.nls"

## How to add a category in Nomenclature:
- Open SPhdThesis.cls 
- Find: 'NOMENCLATURE'
- Then it is quite intuitive, just beware of correct placement of the curly brackets.


# Changing between print and screen mode:
Open file "SPhdThesis.cls" and search in the file for: "screen,print" (around line 19). Value in square brackets, for instance "{screen,print}[screen]", defines the mode.

# Colors
All colors are defined by 'CTU in Prague' house of colors. To see their definion, find 'SECTION: COLORS' in 'SPhdThesis.cls'.

# How to change font color of the headlines?
I used mainly color 'ctu-dark-blue' as headline font color. So in 'SPhdThesis.cls', find all occurances of 'ctu-dark-blue' and replace it by 'ctu-black'.

# Some handy links:
- http://tex.stackexchange.com/questions/13357/fncychap-package-reduce-vertical-gap-space-between-header-and-chapter-heading
- https://tex.stackexchange.com/questions/268406/how-to-color-section-number-and-section-name-with-different-colors/268407#268407
- https://tex.stackexchange.com/questions/8351/what-do-makeatletter-and-makeatother-do
