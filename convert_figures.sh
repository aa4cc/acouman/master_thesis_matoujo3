for f in figs/*.svg
do
    inkscape $f --export-pdf=figs/$(basename -s .svg $f).pdf
done
