\chapter{Introduction}

In this thesis, I present \emph{AcouMan}, a novel platform for contactless acoustophoretic manipulation of multiple objects.
The platform can manipulate spherical objects (balls) and planar objects (thin, 3D-printed shapes) that float in a container filled with water.
The objects are affected by forces that arise from the shape of the surrounding acoustic field.
The field is shaped by a 16-by-16 array of ultrasonic transducers.
The position of the objects is measured, allowing for precise feedback manipulation.

\section{Motivation}

\emph{AcouMan} is the second iteration of an acoustophoretic platform.
In 2018, I wrote and successfully defended a Bachelor's thesis about the first version of the platform \cite{bachelor_thesis}.
A year later, I have co-authored and presented a paper about this version at the \emph{8th IFAC Symposium on Mechatronic Systems} \cite{IFAC_conference}.
Albeit functional, the first version had some drawbacks.
The biggest issue was its small manipulation area and the fact that it was able to control only one object at a time.
Also, the developed control system could only manipulate spherical objects.
Thus, the first version served as a proof of concept.

This thesis aims to fully explore the possibilities of this method of ultrasonic manipulation.
The newly built platform should have a larger manipulation area, should be able to steer multiple objects at once, and it should be able to manipulate floating planar objects, not just spheres.

\section{State of the art}

There are numerous applications that use an array of ultrasonic transducers.
The most prominent example is the acoustic levitation.
A comprehensive overview of progress in the development of acoustic levitation is provided in a paper by \emph{Andrade et al.} \cite{andrade_review_2017}.
In short, there are several methods that utilize both standing \cite{Ochiai2014Pixie,courtney_independent_2014,courtney_dexterous_2013}, and traveling \cite{marzo_holographic_2015} waves and can manipulate objects of various sizes, ranging from microscopic \cite{courtney_independent_2014,courtney_dexterous_2013} to millimeter-scale \cite{Ochiai2014Pixie,marzo_holographic_2015}.
The platforms mentioned above, as well as the majority of others, use open-loop control --- they do not need to measure the position of manipulated objects, because they are naturally stable.
An example of closed-loop control is the so-called \emph{Ultra-Tangibles} platform developed by \emph{Marshall et al.} \cite{marshall_ultra-tangibles:_2012}.
This platform allows for interactive manipulation of multiple objects.
However, compared to \emph{AcouMan}, it is far more complicated --- each ultrasonic transducer has its own microcontroller.

Another example of a platform that uses an array of ultrasonic transducers is the so-called \emph{Ghost Touch} developed by \emph{Marzo et al.} \cite{marzo_ghost_2015}.
This platform is very similar to \emph{AcouMan} in the way it shapes the acoustic field.
The transducers are driven by square-wave voltage signals with appropriate phase shifts to generate acoustic pressure focal points in desired positions.
Controlled positioning of these focal points enables drawing in the sand, inducing flow in a fluid, and to create, move with, or pop bubbles in a soap solution.
It is precisely this effect of focal points on various substances that the \emph{AcouMan} platform attempts to harness for the purpose of contactless manipulation.

\section{Thesis outline}

The thesis is structured in accordance with the assignment.
Each bullet point from the assignment corresponds to one chapter.
In Chapter~\ref{chap:hardware}, I present the mechanical and electronic components of the platform.
In Chapter~\ref{chap:optimization}, I define the optimization problem for shaping the acoustic field and describe an algorithm for solving it.
In Chapter~\ref{chap:balls}, I design a method for independent manipulation of multiple spherical objects.
This procedure involves modeling the spherical object, designing a control system, identifying the parameters of the model, and tuning the control system parameters.
In Chapter~\ref{chap:triangle}, I repeat this design procedure for planar objects.
In Chapter~\ref{chap:assembly}, I present an algorithm for assembling these planar objects into predefined formations.
Finally, I conclude this thesis in Chapter~\ref{chap:conclusion}.
