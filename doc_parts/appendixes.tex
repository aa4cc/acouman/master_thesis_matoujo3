\chapter{Online attachments}

\section{GitLab repository}
\label{sec:gitlab}

The AcouMan project is located at \url{https://gitlab.fel.cvut.cz/aa4cc/acouman}.
The project contains the following relevant repositories:
\begin{itemize}
    \item {\bf AcouMan main:} Main repository. Contains a documentation and submodules with other repositories.
    \item {\bf simulink-controller:} Contains the control systems for spherical and triangular objects implemented in Simulink.
    \item {\bf experiments:} Contains the data as well as the code to replicate the results shown in this thesis.
    \item {\bf Master Thesis:} Contains source files for this thesis.
    \item {\bf matlab:} Contains Matlab scripts for calibrating the platform, optimization, and visualization of the acoustic field.
    \item {\bf manufacturing:} Three repositories containing the drawings, printed circuit board (PCB) designs, and 3D models needed for building the platform.
\end{itemize}

\section{Supplementary video}
\label{sec:video}
The supplementary video is available at \url{https://youtu.be/D5RClzG8gOU}.
The video is split into parts showing manipulation of spherical and triangular objects (see the video description on YouTube).

\chapter{Formulas and derivations}

%This appedix contains derivations of formulas, which are too long, and would disrupt the flow of text.

\section{Deriving the formula for total acoustic pressure}
\label{sec:modulus}

This section and the following one present derivations of efficient formulas for calculating the modulus of acoustic pressure and its spatial derivatives, mentioned in Chapter~\ref{chap:optimization}.

Let us begin with the modulus of acoustic pressure, which is denoted as $|P(x,y,z,\grvect{\Phi})|^2$.
Combining \eqref{eq:total_pressure} and \eqref{eq:pressure_single_transducer}, the squared amplitude of the total pressure can be expressed as:
\begin{equation}
    \label{eq:absolute_pressure_sums}
    |P(x,y,z,\grvect{\Phi})|^2 = \left( \sum_{i=1}^N M^{(i)}(x,y,z) \, \e^{\imag \varphi_i} \right) \, \left( \sum_{i=1}^N \bar{M}^{(i)}(x,y,z) \, \e^{ - \imag \varphi_i} \right) \,,
\end{equation}
where $\bar{M}$ denotes the complex conjugate. 

Let us define vectors $\vect{u} = \left[ \e^{\imag \varphi_1}, \ldots, \e^{\imag \varphi_N} \right]\trans$ and  $\vect{m} = \left[ M^{(1)}(x,y,z), \ldots, M^{(N)}(x,y,z) \right]\trans\,.$ Now, the bracketed terms in \eqref{eq:absolute_pressure_sums} can be expressed and rearranged into:
\begin{equation}
    \label{eq:absolute_pressure_compact}
    |P(x,y,z,\grvect{\Phi})|^2 = \left( \vect{m}\trans\,\vect{u} \right) \, \left( \vect{u}^{\dagger}\,\bar{\vect{m}} \right) = \vect{u}^{\dagger}\,\bar{\vect{m}}\,\vect{m}\trans\,\vect{u}\,,
\end{equation}
where $\vect{u}^{\dagger}$ is the conjugate transpose of $\vect{u}$, and $\bar{\vect{m}}$ is the vector of complex conjugated elements of $\vect{m}$.

While the formula in \eqref{eq:absolute_pressure_compact} is mathematically correct, its numerical implementation involves multiplication of complex vectors, where the imaginary parts cancel out.
Therefore, this formula is not computationally efficient.

Let us therefore further define a matrix $\mat{p} = \left[ \vect{m}_{\mathrm{r}}, \vect{m}_{\mathrm{i}} \right]$ composed of vectors of real $\left(\vect{m}_{\mathrm{r}}\right)$ and imaginary $\left(\vect{m}_{\mathrm{i}}\right)$ parts of $\vect{m}$. Now, the product $\bar{\vect{m}}\,\vect{m}\trans$ can be expressed as:
\begin{equation}
    \label{eq:mmT}
    \bar{\vect{m}}\,\vect{m}\trans = \mat{p}\,\mat{p}\trans + \imag \, \mat{p}\,\begin{bmatrix} 0 & -1 \\ 1 & 0 \end{bmatrix}\,\mat{p}\trans\,,
\end{equation}
while the vector $\vect{u}$ can be expressed as:
\begin{equation}
    \label{eq:vect_u}
    \vect{u} = \vect{c}_{\grvect{\Phi}} + \imag\,\vect{s}_{\grvect{\Phi}}\,,
\end{equation}
where $\vect{c}_{\grvect{\Phi}}$ and $\vect{s}_{\grvect{\Phi}}$ are vectors of cosines and sines of elements from $\grvect{\Phi}$, respecitvely.
Substituting \eqref{eq:mmT} and \eqref{eq:vect_u} into \eqref{eq:absolute_pressure_compact}, we obtain:
\begin{equation}
    \label{eq:absolute_pressure_real}
    \begin{split}
        |P(x,y,z,\grvect{\Phi})|^2 &= \left( \vect{c}_{\grvect{\Phi}}\trans - \imag\,\vect{s}_{\grvect{\Phi}}\trans \right)\,\left( \mat{p}\,\mat{p}\trans + \imag \, \mat{p}\,\begin{bmatrix} 0 & -1 \\ 1 & 0 \end{bmatrix}\,\mat{p}\trans \right)\,\left( \vect{c}_{\grvect{\Phi}} + \imag\,\vect{s}_{\grvect{\Phi}} \right) = \\
            &= \vect{c}_{\grvect{\Phi}}\trans \, \mat{p}\,\mat{p}\trans \, \vect{c}_{\grvect{\Phi}} + \vect{s}_{\grvect{\Phi}}\trans \, \mat{p}\,\mat{p}\trans \, \vect{s}_{\grvect{\Phi}} + \vect{c}_{\grvect{\Phi}}\trans \, \mat{p}\,\begin{bmatrix} 0 & -2 \\ 2 & 0 \end{bmatrix}\,\mat{p}\trans \, \vect{s}_{\grvect{\Phi}}\,.
    \end{split}
\end{equation}

The solver presented in Chapter~\ref{chap:optimization} also requires the gradient of the absolute pressure. Differentiating \eqref{eq:absolute_pressure_real} with respect to $\grvect{\Phi}$ yields:
\begin{equation}
    \label{eq:absolute_pressure_gradient}
    \begin{split}
    \nabla_{\grvect{\Phi}} & |P(x,y,z,\grvect{\Phi})|^2 = - 2 \, \diag{ \vect{s}_{\grvect{\Phi}}} \, \vect{p} \, \vect{p}\trans \, \vect{c}_{\grvect{\Phi}} + 2 \, \diag{ \vect{c}_{\grvect{\Phi}}} \, \vect{p} \, \vect{p}\trans \, \vect{s}_{\grvect{\Phi}} \\ &- \diag{ \vect{s}_{\grvect{\Phi}}} \, \vect{p} \, \begin{bmatrix} 0 & -2 \\ 2 & 0 \end{bmatrix} \, \vect{p}\trans \, \vect{s}_{\grvect{\Phi}} + \diag{ \vect{c}_{\grvect{\Phi}}} \, \vect{p} \, \begin{bmatrix} 0 & -2 \\ 2 & 0 \end{bmatrix} \, \vect{p}\trans \, \vect{c}_{\grvect{\Phi}}\,,
    \end{split}
\end{equation}
where $\mathrm{diag}(.)$ is a diagonal matrix with elements from the given vector. Some terms can be grouped, resulting in the following formula:
\begin{equation}
    \begin{split}
    \nabla_{\grvect{\Phi}} |P(x,y,z,\grvect{\Phi})|^2 =& 2\,\left( \diag{ \vect{c}_{\grvect{\Phi}}} \, \vect{p} \, \begin{bmatrix} 0 & -1 \\ 1 & 0 \end{bmatrix} - \diag{ \vect{s}_{\grvect{\Phi}}} \, \vect{p} \right)\,\vect{p}\trans \, \vect{c}_{\grvect{\Phi}} \\
    & + 2\,\left( \diag{ \vect{c}_{\grvect{\Phi}}} \, \vect{p} - \diag{ \vect{s}_{\grvect{\Phi}}} \, \vect{p} \, \begin{bmatrix} 0 & -1 \\ 1 & 0 \end{bmatrix} \right)\,\vect{p}\trans \, \vect{s}_{\grvect{\Phi}}\,.
    \end{split}
\end{equation}

\section{Calculating spatial derivatives of acoustic pressure}
\label{sec:spatial_derivative}

Let us define a vector $\vect{m}_x = \left[ \frac{\partial\,M^{(1)}(x,y,z)}{\partial\,x}, \ldots, \frac{\partial\,M^{(N)}(x,y,z)}{\partial\,x} \right]\trans$. By differentiating \eqref{eq:absolute_pressure_compact} with respect to $x$, we obtain:
\begin{equation}
    \frac{\partial\,|P|^2}{\partial\,x} = \vect{u}^{\dagger}\,\bar{\vect{m}}_x\,\vect{m}\trans\,\vect{u} + \vect{u}^{\dagger}\,\bar{\vect{m}}\,\vect{m}_x\trans\,\vect{u}\,.
\end{equation}
Notice that the two terms on the right-hand side of the equation above are complex conjugates of each other. Therefore, the spatial derivative is equal to:
\begin{equation}
    \label{eq:spatial_derivative}
    \frac{\partial\,|P|^2}{\partial\,x} = 2 \, \mathfrak{Re} \left\{ \vect{u}^{\dagger}\,\bar{\vect{m}}_x\,\vect{m}\trans\,\vect{u} \right\}\,,
\end{equation}
where $\mathfrak{Re} \left\{.\right\}$ is the real part of a complex number.

Similarly to the previous section, I will first derive all necessary formulas for calculating the spatial derivative, and then provide a formula for efficient calculation.
Let us start with $\vect{m}_x$.
By differentiating \eqref{eq:position_dependent} with respect to $x$, we obtain:
\begin{equation}
    \frac{\partial\,M^{(i)}(x,y,z)}{\partial\,x} = P_0 \left[ \frac{1}{d}\,\frac{\partial\,f_{dir}(x,y,z)}{\partial\,x} + f_{dir}(x,y,z) \left( - \frac{1}{d^2} + \frac{\imag\,k}{d} \right)\frac{\partial\,d}{\partial\,x} \right] \, \e^{\imag k d}\,.
\end{equation}
Since the distance $d$ is defined as $d = \sqrt{x^2 + y^2 + z^2}$, its spatial derivative is:
\begin{equation}
    \frac{\partial\,d}{\partial\,x} = \frac{x}{\sqrt{x^2 + y^2 + z^2}} = \frac{x}{d}\,.
\end{equation}
By applying chain rule to \eqref{eq:directivity}, the spatial derivative of directivity function can be expressed as:
\begin{equation}
    \begin{split}
        \frac{\partial\,f_{dir}(x,y,z)}{\partial\,x} &= \frac{\partial\,f_{dir}(\sin \theta)}{\partial\,(\sin \theta)} \, \frac{\partial\,\sin \theta}{\partial\,x} \\
        &= \left[ \frac{\bessel{0}(k\,r\,\sin\theta)}{\sin\theta} - \frac{2\bessel{1}(k\,r\,\sin\theta)}{k\,r\,\sin^2\theta} - \frac{\bessel{2}(k\,r\,\sin\theta)}{\sin\theta} \right] \, \frac{x\,z^2}{d^3\,\sqrt{x^2+y^2}}\,.
    \end{split} 
\end{equation}

Now, I will do a similar ``trick'' as in the previous section and define a matrix $\mat{p}_x$ composed of real and imaginary parts of $\vect{m}_x$.
The term $\bar{\vect{m}}_x\,\vect{m}\trans$ can be now expressed as:
\begin{equation}
    \bar{\vect{m}}_x\,\vect{m}\trans = \mat{p}_x\,\mat{p}\trans + \imag \, \mat{p}_x\,\begin{bmatrix} 0 & -1 \\ 1 & 0 \end{bmatrix}\,\mat{p}\trans\,,
\end{equation}
and the spatial derivative from \eqref{eq:spatial_derivative} can be expressed as:
\begin{equation}
    \begin{split}
    \frac{\partial\,|P|^2}{\partial\,x} = &2 \, \mathfrak{Re} \left\{ \left( \vect{c}_{\grvect{\Phi}}\trans - \imag\,\vect{s}_{\grvect{\Phi}}\trans \right)\,\left( \mat{p}_x\,\mat{p}\trans + \imag \, \mat{p}_x\,\begin{bmatrix} 0 & -1 \\ 1 & 0 \end{bmatrix}\,\mat{p}\trans\, \right)\,\left( \vect{c}_{\grvect{\Phi}} + \imag\,\vect{s}_{\grvect{\Phi}} \right) \right\} = \\
        = &2 \left( \vect{c}_{\grvect{\Phi}}\trans \, \mat{p}_x\,\mat{p}\trans \, \vect{c}_{\grvect{\Phi}} + \vect{s}_{\grvect{\Phi}}\trans \, \mat{p}_x\,\mat{p}\trans \, \vect{s}_{\grvect{\Phi}} + \vect{c}_{\grvect{\Phi}}\trans \, \mat{p}_x\,\begin{bmatrix} 0 & -1 \\ 1 & 0 \end{bmatrix}\,\mat{p}\trans \, \vect{s}_{\grvect{\Phi}} \right. \\
        &\left. - \vect{s}_{\grvect{\Phi}}\trans \, \mat{p}_x\,\begin{bmatrix} 0 & -1 \\ 1 & 0 \end{bmatrix}\,\mat{p}\trans \, \vect{c}_{\grvect{\Phi}} \right)\,.
    \end{split}    
\end{equation}

The gradient of the spatial derivative is:
\begin{equation}
    \begin{split}
    \nabla_{\grvect{\Phi}}&\left(\frac{\partial\,|P|^2}{\partial\,x}\right) = 2 \, \Bigg( -\diag{\vect{s}_{\grvect{\Phi}}}\left(\mat{p}_x\,\mat{p}\trans + \mat{p}\,\mat{p}_x\trans\right)\vect{c}_{\grvect{\Phi}} \\
    & + \diag{\vect{c}_{\grvect{\Phi}}}\left(\mat{p}_x\,\mat{p}\trans + \mat{p}\,\mat{p}_x\trans\right)\vect{s}_{\grvect{\Phi}} - \diag{\vect{s}_{\grvect{\Phi}}}\,\mat{p}_x\,\begin{bmatrix} 0 & -1 \\ 1 & 0 \end{bmatrix}\,\mat{p}\trans\,\vect{s}_{\grvect{\Phi}} \\
    & + \diag{\vect{c}_{\grvect{\Phi}}}\,\mat{p}\,\begin{bmatrix} 0 & 1 \\ -1 & 0 \end{bmatrix}\,\mat{p}_x\trans\,\vect{c}_{\grvect{\Phi}} - \diag{\vect{c}_{\grvect{\Phi}}}\,\mat{p}_x\,\begin{bmatrix} 0 & -1 \\ 1 & 0 \end{bmatrix}\,\mat{p}\trans\,\vect{c}_{\grvect{\Phi}} \\ 
    & + \diag{\vect{s}_{\grvect{\Phi}}}\,\mat{p}\,\begin{bmatrix} 0 & 1 \\ -1 & 0 \end{bmatrix}\,\mat{p}_x\trans\,\vect{s}_{\grvect{\Phi}} \Bigg)
    \end{split}    
\end{equation}

Due to axial symmetry along the $z$-axis, the spatial derivative with respect to $y$ is almost identical to the spatial derivative with respect to $x$. 

\section{Proving the MPC matrix identity}
\label{sec:MPC_proof}

Recalling \eqref{eq:MPC_HF}, we aim to simplify the calculation of matrices $\mat{H}$ and $\mat{F}$, defined as
\begin{align}
    \mat{H} &= \mathcal{C}^{\rm\,T}\,\bar{\mat{Q}}\,\mathcal{C} + \bar{\mat{R}}\,, &
    \mat{F}^{\rm T} &= \left(\hat{\mat{A}}\,\vect{x}_0 - \vect{X}_r\right)^{\rm T} \bar{\mat{Q}}\,\mathcal{C}\,.
\end{align}
Due to the structure of $\bar{\mat{A}}$, $\bar{\mat{B}}$, and $\grvect{\nu}$ defined in \eqref{eq:triangle_ss_complete}, the following identities hold true:
\begin{align}
    \bar{\mat{A}}^n &= \begin{bmatrix} \bar{\mat{A}}_t^n & & \\ & \bar{\mat{A}}_t^n & \\ & & \bar{\mat{A}}_r^n \end{bmatrix}\,, &
    \bar{\mat{A}}^n\,\bar{\mat{B}}\,\grvect{\nu} &= \begin{bmatrix} \bar{\mat{A}}_t^n\,\bar{\mat{B}}_t\,f_x \\ \bar{\mat{A}}_t^n\,\bar{\mat{B}}_t\,f_y \\ \bar{\mat{A}}_r^n\,\bar{\mat{B}}_r\,\tau \end{bmatrix}\,.
    \label{eq:MPC_identities}
\end{align}
By direct calculation, the element of $\mat{H}$ on $i^{\rm th}$ row and $j^{\rm th}$ column, $\mat{H}_{ij}$ is:
\begin{equation}
\begin{split}
    \mat{H}_{ij} &= \left[ \sum_{k=max(i,j)}^{h_p} \grvect{\nu}^{\rm T}(t+i-1)\,\bar{\mat{B}}^{\rm T}\,\left(\bar{\mat{A}}^{k-i}\right)^{\rm T}\,\mat{Q}\,\bar{\mat{A}}^{k-j}\,\bar{\mat{B}}\,\grvect{\nu}(t+j-1) \right] + \delta_{ij}\,\mat{R}\,,\\
    i,\,j &= 1,\,2,\,\ldots,h_c\,,
\end{split}
\end{equation}
where $\delta_{ij}$ is the Kronecker delta operator.
Substituting the identities from \eqref{eq:MPC_identities}, $\mat{H}_{ij}$ can be expressed using three sums:
\begin{equation}
\begin{split}
    \mat{H}_{ij} &= \left[ \sum_{k=max(i,j)}^{h_p} f_x(t+i-1)\,\bar{\mat{B}}_t^{\rm T}\,\left(\bar{\mat{A}}_t^{k-i}\right)^{\rm T}\,\mat{Q}_t\,\bar{\mat{A}}_t^{k-j}\,\bar{\mat{B}}_t\,f_x(t+j-1) \right] \\
    &+ \left[ \sum_{k=max(i,j)}^{h_p} f_y(t+i-1)\,\bar{\mat{B}}_t^{\rm T}\,\left(\bar{\mat{A}}_t^{k-i}\right)^{\rm T}\,\mat{Q}_t\,\bar{\mat{A}}_t^{k-j}\,\bar{\mat{B}}_t\,f_y(t+j-1) \right] \\
    &+ \left[ \sum_{k=max(i,j)}^{h_p} \tau(t+i-1)\,\bar{\mat{B}}_r^{\rm T}\,\left(\bar{\mat{A}}_r^{k-i}\right)^{\rm T}\,\mat{Q}_r\,\bar{\mat{A}}_r^{k-j}\,\bar{\mat{B}}_r\,\tau(t+j-1) \right] + \delta_{ij}\,\mat{R}\,.
\end{split}
\end{equation}
Therefore, the matrix $\mat{H}$ can be expressed as:
\begin{equation}
    \mat{H} = \mathcal{C}_x^{\rm\,T}\,\bar{\mat{Q}}_t\,\mathcal{C}_x + \mathcal{C}_y^{\rm\,T}\,\bar{\mat{Q}}_t\,\mathcal{C}_y + \mathcal{C}_{\tau}^{\rm\,T}\,\bar{\mat{Q}}_r\,\mathcal{C}_{\tau}\,,
\end{equation}
where
\begin{equation}
    \mathcal{C}_x = \begin{bmatrix} \bar{\mat{B}}_t\,f_x(t) & & &  \\ 
        \bar{\mat{A}}_t\,\bar{\mat{B}}_t\,f_x(t) & \bar{\mat{B}}_t\,f_x(t+1) & &  \\
        \vdots & \vdots & \ddots & \\
        \bar{\mat{A}}_t^{h_c-1}\,\bar{\mat{B}}_t\,f_x(t) & \bar{\mat{A}}_t^{h_c-2}\,\bar{\mat{B}}_t\,f_x(t+1) & \cdots & \bar{\mat{B}}_t\,f_x(t+h_c-1) \\
        \bar{\mat{A}}_t^{h_c}\,\bar{\mat{B}}_t\,f_x(t) & \bar{\mat{A}}_t^{h_c-1}\,\bar{\mat{B}}_t\,f_x(t+1) & \cdots & \bar{\mat{A}}_t\,\bar{\mat{B}}_t\,f_x(t+h_c-1) \\
        \vdots & \vdots & & \vdots \\
        \bar{\mat{A}}_t^{h_p-1}\,\bar{\mat{B}}_t\,f_x(t) & \bar{\mat{A}}_t^{h_p-2}\,\bar{\mat{B}}_t\,f_x(t+1) & \cdots & \bar{\mat{A}}_t^{h_p-h_c}\,\bar{\mat{B}}_t\,f_x(t+h_c-1)
         \end{bmatrix}\,,
\end{equation}
\begin{align}
    \bar{\mat{Q}}_t &= \begin{bmatrix} \mat{Q}_t &  &  \\  & \ddots &  \\  &  & \mat{Q}_t \end{bmatrix}\,,&
    \bar{\mat{Q}}_r &= \begin{bmatrix} \mat{Q}_r &  &  \\  & \ddots &  \\  &  & \mat{Q}_r \end{bmatrix}\,,
\end{align}
and the matrices $\mathcal{C}_y$ and $\mathcal{C}_{\tau}$ are defined analogously using the corresponding state-space matrices and inputs.

Similarly, the $i^{\rm th}$ element of the vector $\mat{F}$ is defined as:
\begin{equation}
    \mat{F}_i = \sum_{k=i}^{h_p} \left(\bar{\mat{A}}^{k}\,\vect{x}_0 - \vect{x}_r(t+k)\right)^{\rm T} \mat{Q}\,\bar{\mat{A}}^{k-j}\,\bar{\mat{B}}\,\grvect{\nu}(t+i-1)\,,
\end{equation}
which can be expressed using three sums:
\begin{equation}
\begin{split}
    \mat{F}_i &= \left[\sum_{k=i}^{h_p} \left(\bar{\mat{A}}_t^{k}\,\vect{x}_{x0} - \vect{x}_{xr}(t+k)\right)^{\rm T} \mat{Q}_t\,\bar{\mat{A}}_t^{k-j}\,\bar{\mat{B}}_t\,f_x(t+i-1)\right] \\
    &+ \left[\sum_{k=i}^{h_p} \left(\bar{\mat{A}}_t^{k}\,\vect{x}_{y0} - \vect{x}_{yr}(t+k)\right)^{\rm T} \mat{Q}_t\,\bar{\mat{A}}_t^{k-j}\,\bar{\mat{B}}_t\,f_y(t+i-1)\right] \\
    &+ \left[\sum_{k=i}^{h_p} \left(\bar{\mat{A}}_r^{k}\,\vect{x}_{\theta 0} - \vect{x}_{\theta r}(t+k)\right)^{\rm T} \mat{Q}_r\,\bar{\mat{A}}_r^{k-j}\,\bar{\mat{B}}_r\,\tau(t+i-1)\right]\,.
\end{split}
\end{equation}
Therefore, the vector $\mat{F}$ can be expressed as:
\begin{equation}
    \mat{F}^{\rm T} = \left(\hat{\mat{A}}_t\,\vect{x}_{x0} - \vect{X}_{xr}\right)^{\rm T} \bar{\mat{Q}}_t\,\mathcal{C}_x + \left(\hat{\mat{A}}_t\,\vect{x}_{y0} - \vect{X}_{yr}\right)^{\rm T} \bar{\mat{Q}}_t\,\mathcal{C}_y + \left(\hat{\mat{A}}_r\,\vect{x}_{\theta 0} - \vect{X}_{\theta r}\right)^{\rm T} \bar{\mat{Q}}_r\,\mathcal{C}_{\tau}\,,
\end{equation}
where
\begin{align}
    \hat{\mat{A}}_t &= \begin{bmatrix} \bar{\mat{A}}_t \\ \bar{\mat{A}}_t^2 \\ \vdots \\ \bar{\mat{A}}_t^{h_p} \end{bmatrix}\,,&
    \vect{x}_{x0} &= \vect{x}_x(t)\,,&
    \vect{X}_{xr} &= \begin{bmatrix} \vect{x}_{xr}(t+1) \\ \vdots \\ \vect{x}_{xr}(t+h_p) \end{bmatrix}\,,
\end{align}
and $\hat{\mat{A}}_t$, $\vect{x}_{y0}$, $\vect{x}_{\theta 0}$, $\vect{X}_{yr}$, and $\vect{X}_{\theta r}$ are defined analogously.
