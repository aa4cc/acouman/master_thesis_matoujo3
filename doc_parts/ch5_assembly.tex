\chapter{Assembling planar objects}
\label{chap:assembly}

In the previous chapter, I have presented a method for manipulation of planar objects.
In this chapter, I describe an algorithm that can assemble these objects into a prescribed formation and present the results.

\section{Assembly algorithm}

To assemble the objects into a formation, we need a higher-level algorithm that outputs reference positions for individual objects based on their current and goal positions.
The proposed algorithm is sequential --- it brings the objects into goal positions one by one based on a predetermined priority.

The priority of the objects is determined by their goal position in the formation.
If the object is on the `outside' of the formation, \abbrv{i.e.}, if it has at least one edge that does not touch any other object, it has a low priority.
If the object is on the `inside' of the formation, \abbrv{i.e.}, if all of its edges touch other objects, it has a high priority.
The actual order among the low- and high-priority objects is arbitrary, as long as the high-priority objects precede the low-priority objects when being assembled.
In the example formation shown in \figref{fig:assembly}, the red triangle with goal position $G_1$ is on the inside, and so it has the highest priority over the green triangles with goal positions $G_2$, $G_3$, and $G_4$.

\begin{figure}[t]
    \vspace{-1em}
    \centering
    \includegraphics[width=0.45\textwidth]{figs/assembly.pdf}
    \caption{An example of a formation of triangular objects. The filled triangles represent the goal positions, the dotted-lined triangles represent the waiting positions.}
    \label{fig:assembly}
    \vspace{-1em}
\end{figure}

Objects with lower priority are initially guided to a `waiting' position.
The waiting position is defined as the goal position offset by a given distance.
The direction of this offset is given by a vector, which is perpendicular to the edge that touches another object, or a sum of these vectors if multiple edges touch other objects.
In the example formation shown in \figref{fig:assembly}, the waiting positions are denoted $W_2$, $W_3$, and $W_4$, and the corresponding perpendicular vectors are denoted $\vect{r}_2$, $\vect{r}_3$, and $\vect{r}_4$.

During the operation, the objects are guided to their waiting, or goal positions based on two simple rules:
\begin{enumerate}
    \item In the beginning, only the object with the highest priority is guided to its goal position. All other objects are guided to their waiting positions.
    \item Guidance of an object is switched from waiting to goal position only if the object has reached its waiting position, and all higher-priority objects have reached their goal positions.
\end{enumerate}
These two rules ensure that the objects are assembled sequentially.
In the example formation shown in \figref{fig:assembly}, these rules mean that the object with goal position $G_1$ is brought to its goal first, followed by $G_2$, then $G_3$, and finally $G_4$.

Having defined the goal and waiting positions, we now need a planning algorithm that guides the objects to these positions.
In fact, we need two distinct algorithms for guidance to the waiting and goal position, because these two actions have contradictory requirements.
When the object is being guided to its waiting position, it needs to avoid contact with other objects, as the planar objects tend to adhere to each other due to surface tension.
On the contrary, objects guided to goal position need to come in contact with other objects in the formation.

To guide the objects to their waiting positions, I employ a potential field planning algorithm, described in \cite{hwang_potential_1992}.
The guided object behaves as if it was exposed to an attractive force towards its waiting position, and to repulsive forces from other objects.
The reference velocities of the $i^{\rm th}$ object, $v_{x_{ri}}$ and $v_{y_{ri}}$, are:
\begin{subequations}
\begin{align}
    v_{x_{ri}}(t) &= c_v\,v_{x_{ri}}(t-1) + c_a\,\left(x_{wi} - x_i(t)\right) + c_r\,\nsum[1.5]_{j \in \{1,\ldots,n\}\setminus\{i\}} \frac{x_i(t) - x_j(t)}{d_{ij}^3(t)}\,, \\
    v_{y_{ri}}(t) &= c_v\,v_{y_{ri}}(t-1) + c_a\,\left(y_{wi} - y_i(t)\right) + c_r\,\nsum[1.5]_{j \in \{1,\ldots,n\}\setminus\{i\}} \frac{y_i(t) - y_j(t)}{d_{ij}^3(t)}\,,
\end{align}
\end{subequations}
where $(x_{wi},\,y_{wi})$ are the coordinates of the waiting position, $c_v$ is the coefficient of retained velocity --- it should be a positive number less than one, $c_a$ is the coefficient of attractive force, $c_r$ is the coefficient of repulsive force, and $d_{ij}$ is the mutual distance between the $i^{\rm th}$ and $j^{\rm th}$ object.
The reference position of the $i^{\rm th}$ object, $(x_{ri},\,y_{ri})$, is then given by:
\begin{align}
    x_{ri}(t) &= x_i(t) + v_{x_{ri}}(t)\,, &
    y_{ri}(t) &= y_i(t) + v_{y_{ri}}(t)\,.
    \label{eq:planner_ref}
\end{align}

To guide the objects to their goal positions, I employ a simpler potential field planner without the repulsive forces.
The reference velocities are then given by:
\begin{subequations}
    \begin{align}
        v_{x_{ri}}(t) &= c_v\,v_{x_{ri}}(t-1) + c_a\,\left(x_{gi} - x_i(t)\right)\,,\\
        v_{y_{ri}}(t) &= c_v\,v_{y_{ri}}(t-1) + c_a\,\left(y_{gi} - y_i(t)\right)\,,
\end{align}
\end{subequations}
where $(x_{wi},\,y_{wi})$ are the coordinates of the goal position.
The reference position is then given by the same formula as in \eqref{eq:planner_ref}.

\section{Results}
\label{sec:ass_results}

The results of the assembly algorithm can be best seen in the supplementary video (see Appendix~\ref{sec:video}).
Selected frames from the video demonstating the assembly of four objects are shown in \figref{fig:stills}.
The measured positions are plotted in \figref{fig:assembly_results}.
As you can see, all four objects are brought to their goal positions within sixteen seconds.

\begin{figure}[hb]
    \vspace{3em}
    \centering
    \includegraphics[width=.95\textwidth]{figs/assembly_results.eps}
    \caption{Results of assembling four triangular objects. The full lines represent the goal positions, the dashed lines represent the measured positions. The assigned priorities (from highest to lowest) are: green, red, yellow, blue.}
    \label{fig:assembly_results}
    \vspace{-1em}
\end{figure}
\FloatBarrier

\begin{figure}[ht]
    \centering
    \begin{subfigure}[t]{0.32 \textwidth}
        \centering        
        \includegraphics[width=\textwidth]{figs/still_0.png}
        \caption{$t=0\,{\rm s}$}
    \end{subfigure}
    \begin{subfigure}[t]{0.32 \textwidth}
        \centering
        \includegraphics[width=\textwidth]{figs/still_1.png}
        \caption{$t=6.68\,{\rm s}$}
    \end{subfigure}
    \begin{subfigure}[t]{0.32 \textwidth}
        \centering
        \includegraphics[width=\textwidth]{figs/still_2.png}
        \caption{$t=9.76\,{\rm s}$}
    \end{subfigure}
    \begin{subfigure}[t]{0.32 \textwidth}
        \centering
        \includegraphics[width=\textwidth]{figs/still_3.png}
        \caption{$t=14.44\,{\rm s}$}
    \end{subfigure}
    \begin{subfigure}[t]{0.32 \textwidth}
        \centering
        \includegraphics[width=\textwidth]{figs/still_4.png}
        \caption{$t=16.84\,{\rm s}$}
    \end{subfigure}
    \caption{Selected frames from the supplementary video showing the process of sequential assembly}
    \label{fig:stills}
\end{figure}