\chapter{Hardware}
\label{chap:hardware}

In this chapter, I briefly describe the mechanical and electronic components of the platform.
As mentioned in the introduction, \emph{AcouMan} is the second iteration of an acoustophoretic platform; the original version was built as a part of my Bachelor's thesis.
A photograph of both platforms is shown in \figref{fig:platforms}.

\section{Mechanical design}

Similarly to its predecessor, the new platform is composed of three laser-engraved plexiglass panels.
The bottom panel serves as a mount for the Raspberry Pi camera module and the LED lights.
The bottom panel of the new version is spray-painted with matte black paint to reduce reflections, and the LED strips illuminating the manipulation area are covered with translucent plastic, which acts as a diffuser.

The middle panel is the manipulation area.
In the previous version, I used a Petri dish as a container for the floating object, as shown in \figref{fig:manipulation_area_old}.
Because the effective manipulation area of the new version is larger, I can no longer use the Petri dish.
To solve this problem, I have built a pool with 3D-printed walls.
Detail of the pool is shown in \figref{fig:manipulation_area_new}.

The top panel holds the array of ultrasonic transducers.
The previous version utilized a laser-cut box as housing, while the new version has the printed circuit board (PCB) with transducers attached directly to the plexiglass panel.
The array of transducers is also larger, increasing from $64$ to $256$ actuators.

\begin{figure}[t]
    \centering
    \begin{subfigure}[t]{0.7 \textwidth}
        \centering
        \includegraphics[width = \textwidth]{figs/platforms_annotated.pdf}
        \caption{A side-by-side comparison between the old (right) and new (left) platform}
        \label{fig:platforms}
    \end{subfigure}
    \begin{subfigure}[t]{0.45 \textwidth}
        \centering
        \includegraphics[width = \textwidth]{figs/manipulation_area_new.jpg}
        \caption{Manipulation area of the new platform}
        \label{fig:manipulation_area_new}
    \end{subfigure}
    \begin{subfigure}[t]{0.3907 \textwidth}
        \centering
        \includegraphics[width = \textwidth]{figs/manipulation_area_old.jpg}
        \caption{Manipulation area of the old platform}
        \label{fig:manipulation_area_old}
    \end{subfigure}
    \caption{Photographs of the old and the new acoustophoretic platform}
\end{figure}

\section{Electronics}

The electronic components of the platform consist of $256$ \emph{Murata MA40S4S} ultrasonic transducers, four signal generators to drive them, and a \emph{Raspberry Pi} computer.
A diagram with connections between these components is shown in \figref{fig:components}.

The \emph{Raspberry Pi} is the central `brain' of the platform.
It processes the images from the camera module\footnote{The computer vision system is available at \url{https://github.com/aa4cc/raspi-ballpos}.}, runs the control algorithm, and communicates with the signal generators.
The control algorithm is implemented in Simulink, allowing for rapid prototyping.
The Simulink models are available on the AcouMan GitLab repository (see Appendix~\ref{sec:gitlab}).

The signal generator is based on the \emph{Terasic DE0-Nano} field-programmable gate array (FPGA) board, and a custom-made `shield' add-on.
The FPGA board generates 64 phase-shifted square-wave signals having $3.3$ volts peak-to-peak (${\rm V}_{\rm pp}$), which are then amplified to $16\,{\rm V}_{\rm pp}$ by the shield.
The phase-shifts and duty cycles of individual signals are set via USB from the \emph{Raspberry Pi}.
The duty cycles are only used to turn individual channels on ($50\,\%$ duty) or off ($0\,\%$ duty), not to regulate the power of the transducers.
The resolution of the phase-shifts is one degree ($\pi/180$ radians).

Since the signals that drive the transducers are generated on four separate FPGA boards, the boards need to be synchronized.
The synchronization method as well as the FPGA program was designed by an undergraduate student Petr Brož\footnote{The code is available at \url{https://github.com/aa4cc/fpga-generator}.}.
His solution employs a master-slave architecture.
One FPGA board, configured as the master, transmits high-frequency pulses, which mark the rising edge of a zero phase-shift signal.
Other boards receive these pulses and adjust accordingly.
When the FPGA boards receive the new set of phase-shifts and duty cycles, they do not reconfigure their outputs immediatelly.
Instead, they wait for a trigger pulse transmitted from one of the general purpose input-output (GPIO) ports of the \emph{Raspberry Pi}.
Therefore, the driving signals change simultaneously.
In addition, the FPGA generators also transmit an acknowledgement whether they received the new set of phase-shifts or duty cycles correctly.

My contribution consists mainly of the communication script that runs on the \emph{Raspberry Pi}.
The script has a defined number of communication attempts.
It transmits the given set of phase-shifts and duty cycles to every generator until it receives an acknowledgement or runs out of attempts.
Upon transmitting the last message, the script sends the trigger pulse to the generators.

\begin{figure}[t]
    \centering
    \includegraphics[width=.66\textwidth]{figs/components.pdf}
    \caption{Principal signal diagram of the platform}
    \label{fig:components}
\end{figure}