\chapter[Manipulation of planar objects]{Manipulation of planar\\ objects}
\label{chap:triangle}

In this chapter, I describe the modifications to the model and the control system needed to manipulate planar objects.
As a test subject, I am using an isosceles triangle, 3D-printed from thermoplastic polyurethane (TPU 95A) with a \SI{2}{\milli\meter} thickness.
For modeling and control design, I am using only the `amplitude fitting' method, since it has proven to be more precise than the `gradient fitting' method.

\section{Mathematical model}

First, let us establish some conventions regarding the triangular object.
Let us denote its vertices as $E$, $F$, and $G$, with the side $\overline{EF}$ being the base and the vertex $G$ being the apex.
The size of the triangle is given by the length of its base, $l$, and its height, $h$.
Let $H$ be the centroid of the triangle.
The position of the triangle is defined by the coordinates of the centroid, $x$ and $y$, and its orientation, $\theta$, defined as the angle of the vector $\overrightarrow{HG}$.
The conventions are illustrated in \figref{fig:trigeom}.

Let us, once more, use Newton's second law to obtain the dynamical equations of the object.
Furthermore, let us assume that the drag force and torque are proportional to the velocity and the angular speed, respectively.
Therefore, the resulting dynamical equations are:
\begin{align}
    m\,\ddot{x} &= - c_{dt}\,\dot{x} + F_x\,, &
    m\,\ddot{y} &= - c_{dt}\,\dot{y} + F_y\,, &
    J\,\ddot{\theta} &= - c_{dr}\,\dot{\theta} + T\,,
    \label{eq:triangle_newton}
\end{align}
where $m$ is the mass of the object, $J$ is the moment of inertia, $c_{dt}$ and $c_{dr}$ are the translational and rotational coefficients of drag, respectively, $F_x$ and $F_y$ are the components of the acoustophoretic force, and $T$ is the torque induced by the acoustophoretic force.

Having the dynamical equations \eqref{eq:triangle_newton}, it remains to figure out how the pressure field relates to the net forces $F_x$, $F_y$, and net torque $T$ acting on the manipulated object.
For this purpose, we can view the acoustophoretic force as analogous to the Coulomb force.
Let $(x_p,y_p)$ be the coordinates of a high-pressure point with an amplitude $P$.
Let $\Delta P$ be the amplitude of pressure after applying the dead zone introduced in \eqref{eq:amplitude_fitting_force}, \abbrv{i.e.},
\begin{equation}
    \Delta P = \max\left(P - 800, 0\right)\,.
\end{equation}
Let us consider an infinitesimally small element of the mass of the triangular object, ${\rm d}m$, with coordinates $(x_m,y_m)$.
Then, the acoustophoretic force ${\rm d}{\bf F} = [{\rm d}F_x,\, {\rm d}F_y ]^{\rm T}$ and torque ${\rm d}T$ acting on this element are proportional to:
\begin{align}
    {\rm d}{\bf F} &\propto \frac{\Delta P}{\| {\bf r}_p \|^3} {\bf r}_p\,{\rm d}m\,,&
    {\rm d}T &\propto \det \begin{bmatrix} {\bf r}_m & \frac{\Delta P}{\| {\bf r}_p \|^3} {\bf r}_p \end{bmatrix}\,{\rm d}m\,,
    \label{eq:triangle_force_torque}
\end{align}
where ${\bf r}_m = [x_m-x,\, y_m-y]^{\rm T}$ and ${\bf r}_p = [x_p-x,\, y_p-y]^{\rm T}$.
An illustration of this model of forces is shown in \figref{fig:triforce}.

\begin{figure}[t]
    \centering
    \begin{subfigure}[t]{0.49 \textwidth}
        \centering
        \includegraphics[width=0.65\textwidth]{figs/trigeom.pdf}
        \caption{Definition of position and orientation}
        \label{fig:trigeom}
    \end{subfigure}
    \begin{subfigure}[t]{0.49 \textwidth}
        \centering
        \includegraphics[width=0.65\textwidth]{figs/triforce.pdf}
        \caption{Vectors used in the calculation of forces}
        \label{fig:triforce}
    \end{subfigure}
    \caption{Geometry of the triangle}
    \label{fig:triangle}
    \vspace{-1em}
\end{figure}

According to \eqref{eq:triangle_force_torque}, the acoustophoretic force and torque are proportional to $\Delta P$.
Thus, let us define a normalized `pseudo-force', ${\bf f} = [f_x,\,f_y]^{\rm T}$, and `pseudo-torque', $\tau$, that are independent on $\Delta P$.
The net acoustophoretic force and torque are then given by:
\begin{align}
    F_x &\propto f_x \, \Delta P\,,&
    F_y &\propto f_y \, \Delta P\,,&
    T &\propto \tau \, \Delta P\,,
\end{align}
To determine $\vect{f}$ and $\tau$, we need to integrate \eqref{eq:triangle_force_torque} with $\Delta P=1$.
For that, I propose three models. 

The first model assumes that the acoustophoretic force affects the full volume of the triangular object.
Therefore, ${\bf f}$ and $\tau$ can be expressed using the following integrals:
\begin{align}
    {\bf f} &= \iint_{\triangle EFG} \frac{1}{\| {\bf r}_p \|^3} {\bf r}_p\,{\rm d}y_m\,{\rm d}x_m\,,&
    \tau &= \iint_{\triangle EFG} \det \begin{bmatrix} {\bf r}_m & \frac{1}{\| {\bf r}_p \|^3} {\bf r}_p \end{bmatrix}\,{\rm d}y_m\,{\rm d}x_m\,,
    \label{eq:full_volume}
\end{align}
where the double integral denotes integration over the surface of the triangle $EFG$.

The second model assumes that the acoustophoretic force affects only edges, which are exposed to the high-pressure point.
For instance, in the situation shown in \figref{fig:triforce}, the only exposed edge is $\overline{EF}$.
In that situation, ${\bf f}$ and $\tau$ can be expressed using the following integrals:
\begin{subequations}
\begin{align}
    {\bf f} &= \int_{0}^{1} \frac{1}{\| {\bf r}_p \|^3} {\bf r}_p\,{\rm d}\rho\,,&
    x_m &= (1 - \rho)\,x_E + \rho\,x_F\,, \\
    \tau &= \int_{0}^{1} \det \begin{bmatrix} {\bf r}_m & \frac{1}{\| {\bf r}_p \|^3} {\bf r}_p \end{bmatrix}\,{\rm d}\rho\,,&
    y_m &= (1 - \rho)\,y_E + \rho\,y_F\,,
\end{align}
\label{eq:along_edge}
\end{subequations}
where $(x_E,\,y_E)$ are the coordinates of the vertex $E$, and $(x_F,\,y_F)$ are the coordinates of the vertex $F$.

The third model assumes that the acoustophoretic force affects only the nearest point on the perimeter of the triangle.
Therefore, ${\bf f}$ and $\tau$ can be expressed as:
\begin{align}
    {\bf f} &= \frac{1}{\| {\bf r}_p \|^3} {\bf r}_p\,,&
    \tau &= \det \begin{bmatrix} {\bf r}_m & \frac{1}{\| {\bf r}_p \|^3} {\bf r}_p \end{bmatrix}\,,&
    \begin{bmatrix} x_m \\ y_m \end{bmatrix} &= \argmin_{[x_m,\,y_m]^{\rm T} \in \triangle EFG} \left\| \begin{bmatrix} x_p - x_m \\ y_p - y_m \end{bmatrix} \right\|_2\,.
    \label{eq:closest_point}
\end{align}

These three models are shown in \figref{fig:triangle_forces}.
The blue arrows represent the direction and magnitude of the `pseudo-force' for different high-pressure points around the triangle.
The red markers represent the coordinates of the high-pressure points, as well as the direction and magnitude of the `pseudo-torque' --- a circle means positive torque, a cross means negative torque, and the size of the marker increases with the magnitude.

\begin{figure}[t]
    \vspace{-0.5em}
    \centering
    \includegraphics[width=.99\textwidth]{figs/triangle_forces.pdf}
    \caption{Models of pseudo-forces and pseudo-torques}
    \label{fig:triangle_forces}
    \vspace{-2.5em}
\end{figure}

Let us define three sets of states, ${\bf x}_x = [v_x,\,x]^{\rm T}$, ${\bf x}_y = [v_y,\,y]^{\rm T}$, and ${\bf x}_{\theta} = [\omega,\,\theta]^{\rm T}$, where $v_x = \dot{x}$, $v_y = \dot{y}$, and $\omega = \dot{\theta}$.
Combining the models of the `pseudo-force' and `pseudo-torque' with the dynamical equations from \eqref{eq:triangle_newton} yields the following state-space description:
\begin{subequations}
\begin{align}
    \dot{\bf x}_x &= \mat{A}_t\,{\bf x}_x + \mat{B}_t\,f_x\,\Delta P\,, &
    x &= \mat{C}\,{\bf x}_x\,, \\
    \dot{\bf x}_y &= \mat{A}_t\,{\bf x}_y + \mat{B}_t\,f_y\,\Delta P\,, &
    y &= \mat{C}\,{\bf x}_y\,, \\
    \dot{\bf x}_{\theta} &= \mat{A}_r\,{\bf x}_{\theta} + \mat{B}_r\,\tau\,\Delta P\,, &
    \theta &= \mat{C}\,{\bf x}_x\,,
\end{align}
\label{eq:triangle_ss}
\end{subequations}
where $\mat{C} = [0 \,\, 1]$, and the matrices $\mat{A}_t$, $\mat{A}_r$, $\mat{B}_t$, and $\mat{B}_r$ are in the same form as in \eqref{eq:ABform}, \abbrv{i.e.},

\begin{align}
    \mat{A} &= \begin{bmatrix} -d & 0 \\ 1 & 0 \end{bmatrix}\,, &
    \mat{B} &= \begin{bmatrix} g \\ 0 \end{bmatrix}\,.
\end{align}

\section{Control system}

The control system for triangular objects has the same structure as the control system for spherical objects presented in Section~\ref{sec:control_system}.
The major difference between the systems is in the controller.
I propose two controllers, an LQR-based, and an MPC-based controller.

\subsection{LQR}

Let $\vect{x}_{xr}$, $\vect{x}_{yr}$ and $\vect{x}_{\theta r}$ be the reference setpoints.
Furthermore, let us define errors $\tilde{\vect{x}}_x$, $\tilde{\vect{x}}_y$ and $\tilde{\vect{x}}_{\theta}$ as:
\begin{align}
    \tilde{\vect{x}}_{x} &= \vect{x}_{x} - \vect{x}_{xr}\,,&
    \tilde{\vect{x}}_{y} &= \vect{x}_{y} - \vect{x}_{yr}\,,&
    \tilde{\vect{x}}_{\theta} &= \vect{x}_{\theta} - \vect{x}_{\theta r}\,.
\end{align}
Now, let us define the LQR control actions as:
\begin{align}
    u_{x} &= - \vect{K}_t\,\tilde{\vect{x}}_{x}\,,&
    u_{y} &= - \vect{K}_t\,\tilde{\vect{x}}_{y}\,,&
    u_{\theta} &= - \vect{K}_r\,\tilde{\vect{x}}_{\theta}\,,
\end{align}
where $\vect{K}_t$ and $\vect{K}_r$ are state feedback gain for the translational and rotational systems, respectively.
From \eqref{eq:triangle_ss}, it follows that the control inputs should be equal to:
\begin{align}
    u_{x} &= f_{x}\,\Delta P\,,&
    u_{y} &= f_{y}\,\Delta P\,,&
    u_{\theta} &= \tau\,\Delta P\,.
    \label{eq:triangle_ufDeltaP}
\end{align}
The `pseudo-force' and `pseudo-torque' depend on the position of the high-pressure point.
Therefore, we need to find $x_p$, $y_p$ and $\Delta P$ that satisfy \eqref{eq:triangle_ufDeltaP}.
Since the formulas for the `pseudo-force' and `pseudo-torque' in \eqref{eq:full_volume}, \eqref{eq:along_edge} and \eqref{eq:closest_point} are too complex to be optimized in real-time, I use a lookup table.
I take $m$ evenly spaced points around the triangle (similarly as in \figref{fig:triangle_forces}) and calculate the values of $\vect{f}$ and $\tau$ for each point.
Therefore, each entry of the lookup table is a five-tuple
\begin{align}
    & \left(x_{p,i},\,y_{p,i}\,,f_{x,i}\,,f_{y,i}\,,\tau_i\right)\,, & i &= 1,\,2,\,\ldots,\,m\,.
\end{align}
The values of the `pseudo-forces' and `pseudo-torques' are pre-computed for a triangle situated at the origin with its orientation equal to zero.
Therefore, the LQR control inputs need to be transformed into $\bar{u}_x$ and $\bar{u}_y$ so that
\begin{equation}
    \begin{bmatrix} \bar{u}_x \\ \bar{u}_y \end{bmatrix} = \begin{bmatrix} \cos{\theta} & \sin{\theta} \\ -\sin{\theta} & \cos{\theta} \end{bmatrix} \, \begin{bmatrix} u_x \\ u_y \end{bmatrix}\,.
\end{equation}
Finding the optimal position and amplitude of the high-pressure point involves solving the following mixed-integer program:
\begin{subequations}
\begin{align}
    i^*,\,\Delta P^* = &\argmin_{i \in \mathbb{N},\,\Delta P \in \mathbb{R}} \left[ \left(\bar{u}_x - f_{xi}\,\Delta P \right)^2 + \left(\bar{u}_y - f_{yi}\,\Delta P \right)^2 + w_{\tau} \left( u_{\theta} - \tau_i\,\Delta P \right)^2 \right]\,, \label{eq:triangle_criterion}\\
    &\text{subject to }\, i \in \left\{1,\,2,\,\ldots,\,m\right\}\,,\\
    & \quad \quad \quad \quad \,\,\,\, 0 \leq \Delta P \leq 1700\,,
\end{align}
\label{eq:LQR_opt}
\end{subequations}
where $w_{\tau}$ is a torque weight.
The `pseudo-forces' and `pseudo-torques' are sampled at $m = 300$ points.
The relatively small size of the problem allows the use of a brute force approach:
we enumerate all i.
For each i, we compute the optimal amplitude $\Delta P^*_i$, which is given by the saturated weighted average
\begin{equation}
    \Delta P^*_i = \max \left( \min \left( \frac{\frac{\bar{u}_x}{f_{x,i}} + \frac{\bar{u}_y}{f_{y,i}} + \sqrt{w_\tau}\,\frac{\bar{u}_{\theta}}{\tau_i}}{2 + \sqrt{w_{\tau}}}\,, 1700 \right),\, 0 \right)\,,
\end{equation}
and store the corresponding value of the cost function.
The solution of \eqref{eq:LQR_opt} is then readily given by $i$ and $\Delta P^*_i$ resulting in the smallest value of the cost function.

The final coordinates of the high-pressure point are obtained form the following transformation:
\begin{equation}
\begin{bmatrix} x_p \\ y_p \end{bmatrix} = \begin{bmatrix} x \\ y \end{bmatrix} + \begin{bmatrix} \cos{\theta} & -\sin{\theta} \\ \sin{\theta} & \cos{\theta} \end{bmatrix} \, \begin{bmatrix} x_{p,i^*} \\ y_{p,i^*} \end{bmatrix}\,,
\end{equation}
and the amplitude of the high-pressure point is:
\begin{equation}
    P = \Delta P^* + 800\,.
\end{equation}

\subsection{MPC}

Let us define a complete state of the triangular object $\vect{x} = \left[\vect{x}_x^{\rm T} \,\, \vect{x}_y^{\rm T} \,\, \vect{x}_{\theta}^{\rm T}\right]^{\rm T}$.
The discrete-time state-space description of the system is then given by
\begin{equation}
    \vect{x}(t+1) = \underbrace{\begin{bmatrix} \bar{\mat{A}}_t & 0 & 0 \\ 0 & \bar{\mat{A}}_t & 0 \\ 0 & 0 & \bar{\mat{A}}_r \end{bmatrix}}_{\bar{\mat{A}}}\,\vect{x}(t) + \underbrace{\begin{bmatrix} \bar{\mat{B}}_t & 0 & 0 \\ 0 & \bar{\mat{B}}_t & 0 \\ 0 & 0 & \bar{\mat{B}}_r \end{bmatrix}}_{\bar{\mat{B}}}\,\underbrace{\begin{bmatrix} f_{x}(t) \\ f_{y}(t) \\ \tau(t) \end{bmatrix}}_{\grvect{\nu}(t)}\,\Delta P(t)\,,
    \label{eq:triangle_ss_complete}
\end{equation}
where $\bar{\mat{A}}_t$, $\bar{\mat{A}}_r$, $\bar{\mat{B}}_t$, and $\bar{\mat{B}}_r$ are ZOH-discretized state matrices from \eqref{eq:triangle_ss}.

Let $h_p$ be the length of the prediction horizon and let $h_c$ be the length of the control horizon of the MPC ($h_p \geq h_c$).
Furthermore, let us define a complete error of the triangular object $\tilde{\vect{x}} = \left[\tilde{\vect{x}}_x^{\rm T} \,\, \tilde{\vect{x}}_y^{\rm T} \,\, \tilde{\vect{x}}_{\theta}^{\rm T}\right]^{\rm T}$.
The goal of model predictive control is to minimize the following criterion:
\begin{equation}
    J = \frac{1}{2} \sum_{i = 1}^{h_p} \tilde{\vect{x}}^{\rm T}(t+i)\,\mat{Q}\,\tilde{\vect{x}}(t+i) + 
           \frac{1}{2} \sum_{i = 0}^{h_c-1} \mat{R}\,\left(\Delta P(t+i)\right)^2\,,
\end{equation}
where $\mat{Q}$ is a 6-by-6 positive definite matrix and $\mat{R}$ is a positive scalar.

Now, let us express the criterion in a compact matrix form.
To do so, let us define a stacked vector of states, $\vect{X}$, a stacked vector of references, $\vect{X}_r$, and a stacked vector of amplitudes, $\vect{P}$:
\begin{align}
    \vect{X} &= \begin{bmatrix} \vect{x}(t+1) \\ \vdots \\ \vect{x}(t+h_p) \end{bmatrix}\,,&
    \vect{X}_r &= \begin{bmatrix} \vect{x}_r(t+1) \\ \vdots \\ \vect{x}_r(t+h_p) \end{bmatrix}\,,&
    \vect{P} &= \begin{bmatrix} \Delta P(t) \\ \vdots \\ \Delta P(t+h_c-1) \end{bmatrix}\,,
\end{align}
where $\vect{x}_r(t+i) = \vect{x}_r(t)$ if the future references are not known.
Let us also define two matrices
\begin{align}
    \bar{\mat{Q}} &= \underbrace{\begin{bmatrix} \mat{Q} &  &  \\  & \ddots &  \\  &  & \mat{Q} \end{bmatrix}}_{h_p \text{ times}}\,,&
    \bar{\mat{R}} &= \underbrace{\begin{bmatrix} \mat{R} &  &  \\  & \ddots &  \\  &  & \mat{R} \end{bmatrix}}_{h_c \text{ times}}\,.
\end{align}
Now, the criterion $J$ can be expressed in a compact matrix form:
\begin{equation}
    J = \left(\vect{X} - \vect{X}_r\right)^{\rm T}\,\bar{\mat{Q}}\,\left(\vect{X} - \vect{X}_r\right) + \vect{P}^{\rm T}\,\bar{\mat{R}}\,\vect{P}\,.
    \label{eq:MPC_criterion}
\end{equation}

The system presented in \eqref{eq:triangle_ss_complete} is nonlinear due to the multiplication of inputs $\grvect{\nu}$ and $\Delta P$.
However, if the sequence of inputs $\grvect{\nu}(t)$ is fixed, the system becomes linear time-varying.
For the moment, let us postpone the discssuion on how to choose the sequence $\grvect{\nu}(t)$ and assume that we have already fixed it.
Then, the goal of the MPC is to find an optimal sequence of amplitudes, $\vect{P}^*$, so that:
\begin{subequations}
\begin{align}
    \vect{P}^* &= \argmin_{\vect{P} \in \mathbb{R}^{h_c}} J\,, \\
    \text{subject to } \vect{x}(k+1) &= \begin{cases}
        \bar{\mat{A}}\,\vect{x}(k) + \bar{\mat{B}}\,\grvect{\nu}(k)\,\Delta P(k)\,,&  k = t,\,\ldots,\,t+h_c-1\,, \\
        \bar{\mat{A}}\,\vect{x}(k)\,,& k = t+h_c,\,\ldots,\,t+h_p-1\,,
    \end{cases} \\
    \vect{x}(t) &= \vect{x}_0\,, \quad
    \begin{bmatrix} 0 \\ \vdots \\ 0 \end{bmatrix} \leq \vect{P} \leq \begin{bmatrix} 1700 \\ \vdots \\ 1700 \end{bmatrix}\,.
\end{align}
\label{eq:sparse_MPC}
\end{subequations}

The optimization problem presented in \eqref{eq:sparse_MPC} is the so-called sparse MPC formulation, which contains equality constraints.
We can eliminate these constraints by deriving the condensed MPC formulation \cite{wright_MPC_2019}.
First, we need to express the stacked states.
From the state-space equations, we obtain
\begin{equation}
    \vect{X} = \hat{\mat{A}}\,\vect{x}_0 + \mathcal{C}\,\vect{P}\,,
    \label{eq:stacked_state_eq}
\end{equation}
where
\begin{align}
    \hat{\mat{A}} &= \begin{bmatrix} \bar{\mat{A}} \\ \bar{\mat{A}}^2 \\ \vdots \\ \bar{\mat{A}}^{h_p} \end{bmatrix}\,,&
    \mathcal{C} &= \begin{bmatrix} \bar{\mat{B}}\,\grvect{\nu}(t) & & &  \\ 
        \bar{\mat{A}}\,\bar{\mat{B}}\,\grvect{\nu}(t) & \bar{\mat{B}}\,\grvect{\nu}(t+1) & &  \\
        \vdots & \vdots & \ddots & \\
        \bar{\mat{A}}^{h_c-1}\,\bar{\mat{B}}\,\grvect{\nu}(t) & \bar{\mat{A}}^{h_c-2}\,\bar{\mat{B}}\,\grvect{\nu}(t+1) & \cdots & \bar{\mat{B}}\,\grvect{\nu}(t+h_c-1) \\
        \bar{\mat{A}}^{h_c}\,\bar{\mat{B}}\,\grvect{\nu}(t) & \bar{\mat{A}}^{h_c-1}\,\bar{\mat{B}}\,\grvect{\nu}(t+1) & \cdots & \bar{\mat{A}}\,\bar{\mat{B}}\,\grvect{\nu}(t+h_c-1) \\
        \vdots & \vdots & & \vdots \\
        \bar{\mat{A}}^{h_p-1}\,\bar{\mat{B}}\,\grvect{\nu}(t) & \bar{\mat{A}}^{h_p-2}\,\bar{\mat{B}}\,\grvect{\nu}(t+1) & \cdots & \bar{\mat{A}}^{h_p-h_c}\,\bar{\mat{B}}\,\grvect{\nu}(t+h_c-1)
         \end{bmatrix}\,.
\end{align}
Substituting \eqref{eq:stacked_state_eq} to \eqref{eq:MPC_criterion} yields:
\begin{equation}
    J = \frac{1}{2} \vect{P}^{\rm T}\,\mathcal{C}^{\rm\,T}\,\bar{\mat{Q}}\,\mathcal{C}\,\vect{P} + \left(\hat{\mat{A}}\,\vect{x}_0 - \vect{X}_r\right)^{\rm T} \bar{\mat{Q}}\,\mathcal{C}\,\vect{P} + \underline{\left(\hat{\mat{A}} \vect{x}_0 - \vect{X}_r\right)^{\rm T} \bar{\mat{Q}} \left(\hat{\mat{A}} \vect{x}_0 - \vect{X}_r\right)} + \frac{1}{2} \vect{P}^{\rm T}\,\bar{\mat{R}}\,\vect{P}\,.
\end{equation}
The underlined term is constant with respect to the optimized variable and can be therefore omitted in the criterion.
Thus, the optimal sequence of amplitudes is obtained by solving the following quadratic program:
\begin{subequations}
\begin{align}
    \vect{P}^* &= \argmin_{\vect{P} \in \mathbb{R}^{h_c}} \frac{1}{2} \vect{P}^{\rm T}\,\mat{H}\,\vect{P} + \mat{F}^{\rm T}\,\vect{P}\,, \\
    &\text{subject to } \begin{bmatrix} 0 \\ \vdots \\ 0 \end{bmatrix} \leq \vect{P} \leq \begin{bmatrix} 1700 \\ \vdots \\ 1700 \end{bmatrix}\,,
\end{align}
\end{subequations}
where
\begin{align}
    \mat{H} &= \mathcal{C}^{\rm\,T}\,\bar{\mat{Q}}\,\mathcal{C} + \bar{\mat{R}}\,, &
    \mat{F}^{\rm T} &= \left(\hat{\mat{A}}\,\vect{x}_0 - \vect{X}_r\right)^{\rm T} \bar{\mat{Q}}\,\mathcal{C}\,.
    \label{eq:MPC_HF}
\end{align}

Note that the matrices of the state-space representation in \eqref{eq:triangle_ss_complete} are block diagonal.
This property means that $\hat{\mat{A}}$ and $\mathcal{C}$ are sparse --- they are, effectively, blocks of block diagonal matrices.
Consequently, calculating $\mat{H}$ and $\mat{F}$ is too lengthy due to the multiplication of many zero elements.

However, if the matrix $\mat{Q}$ is in a block diagonal form
\begin{equation}
    \mat{Q} = \begin{bmatrix} \mat{Q}_t & 0 & 0 \\ 0 & \mat{Q}_t & 0 \\ 0 & 0 & \mat{Q}_r \end{bmatrix}\,,
\end{equation}
we can express $\mat{H}$ and $\mat{F}$ using smaller, dense matrices:
\begin{subequations}
\begin{align}
    \mat{H} &= \mathcal{C}_x^{\rm\,T}\,\bar{\mat{Q}}_t\,\mathcal{C} _x + \mathcal{C}_y^{\rm\,T}\,\bar{\mat{Q}}_t\,\mathcal{C}_y + \mathcal{C}_{\tau}^{\rm\,T}\,\bar{\mat{Q}}_r\,\mathcal{C}_{\tau}\,, \\
    \mat{F}^{\rm T} &= \left(\hat{\mat{A}}_t\,\vect{x}_{x0} - \vect{X}_{xr}\right)^{\rm T} \bar{\mat{Q}}_t\,\mathcal{C}_x + \left(\hat{\mat{A}}_t\,\vect{x}_{y0} - \vect{X}_{yr}\right)^{\rm T} \bar{\mat{Q}}_t\,\mathcal{C}_y + \left(\hat{\mat{A}}_r\,\vect{x}_{\theta 0} - \vect{X}_{\theta r}\right)^{\rm T} \bar{\mat{Q}}_r\,\mathcal{C}_{\tau}\,,
\end{align}
\end{subequations}
where $\hat{\mat{A}}_t$, $\hat{\mat{A}}_r$, $\mathcal{C}_x$, $\mathcal{C}_y$, $\mathcal{C}_{\tau}$, $\bar{\mat{Q}}_t$, and $\bar{\mat{Q}}_r$ have the same structure as $\hat{\mat{A}}$, $\mathcal{C}$, and $\bar{\mat{Q}}$, but use the smaller matrices of the translational and rotational subsystems (proof in Appendix~\ref{sec:MPC_proof}).

Having defined a quadratic program as well as a method for efficient calculation of its matrices, we now need a tool for solving it.
To solve the quadratic program, I have implemented the accelerated gradient projection method, described in \cite{giselsson_accelgrad_2013}.
This method has proven to converge to the optimal solution fast, and is easily implemented for quadratic programs with box constraints.
The method iteratively searches for the optimal value of $\vect{P}$.
Let $\vect{P}_k$ be the guess at $k^{\rm th}$ iteration.
The method then defines a `momentum' for the next iteration as:
\begin{equation}
    \vect{s}_{k+1} = \vect{P}_k + \beta_k \left(\vect{P}_k - \vect{P}_{k-1}\right)\,,
\end{equation}
where $\beta_k$ is a coefficient that should be equal to zero for $k=0$ and gradually increase to one, \abbrv{e.g.},
\begin{equation}
    \beta_k = \begin{cases} 0\,, & k=0\,, \\ \frac{k-1}{k+2}\,, & k>0\,. \end{cases}
\end{equation}
Then, the next value of $\vect{P}$ is given by:
\begin{equation}
    \vect{P}_{k+1} = \max \left(\min \left(\vect{s}_{k+1} - \lambda\left(\mat{H}\,\vect{s}_{k+1} + \mat{F}\right),\,1700\right),\,0\right)\,,
\end{equation}
where $\lambda$ is the step size.

So far, we tried to find an optimal sequence of amplitudes for a given sequence of $\grvect{\nu}(t)$, \abbrv{i.e.}, the `pseudo-forces' and `pseudo-torques.'
Now, we aim to find the optimal sequence of $\grvect{\nu}(t)$.
Similarly to the LQR, we use a lookup table, which reduces the problem to finding an optimal array of lookup table indices.
Unlike the LQR, a brute force approach is not viable since the number of all possible combinations grows exponentially with the length of the control horizon.
To solve this problem, I employ a particle swarm optimization (PSO) algorithm, described in \cite{poli_particle_2007}.
The algorithm is iterative, and works as follows: we have a population of particles $\vect{z}_1,\,\vect{z}_2,\,\ldots,\,\vect{z}_n$.
Each particle represents a solution (in our case, a sequence of indices, which will define our $\grvect{\nu}(t)$).
In addition, the particles have velocities $\vect{v}_1,\,\vect{v}_2,\,\ldots,\,\vect{v}_n$ --- vectors of the same dimension as the particles.
Both the solutions and the velocities are initialized randomly.
In every iteration, the particles are updated using the following formula:
\begin{subequations}
\begin{align}
    \vect{v}_i^{(k+1)} &= w_k\,\vect{v}_i^{(k)} + \mathcal{U}(0,2)\,\left(\vect{b}_i - \vect{z}_i^{(k)}\right) + \mathcal{U}(0,2)\,\left(\vect{b}_g - \vect{z}_i^{(k)}\right)\,,\\
    \vect{z}_i^{(k+1)} &= \vect{z}_i^{(k)} + \vect{v}_i^{(k)}\,,
\end{align}
\end{subequations}
where $w_k$ is the `inertia weight,' $\mathcal{U}(0,2)$ is a random uniformly distributed number between $0$ and $2$, $\vect{b}_i$ is the best recorded solution (solution that results in the smallest value of the criterion $J$) for the $i^{\rm th}$ particle, and $\vect{b}_g$ is the globally best solution.
The value of $w_k$ should initially be relatively high (\abbrv{e.g.}, $0.9$) and gradually decrease (\abbrv{e.g.}, to $0.4$).
Note that since the solution should be positive integers, the values of $\vect{z}_i$ are rounded.
Also, we need to use modulo arithmetics so that the values of $\vect{z}_i$ remain between $1$ and $m$ (the number of entries in the lookup table).

After a fixed number of iterations, we take $\vect{b}_g$ as the optimal sequence of high-pressure points, together with the corresponding sequence of amplitudes.
However, we only use the first point of this sequence, since the whole MPC algorithm is repeated every control period.
Consequently, solving for $\grvect{\nu}(t)$ using PSO requires solving $n$ quadratic programs (one for each particle) every iteration.
In the current setting, PSO performs seven iterations with five particles, which results in solving $35$ quadratic programs every control period.

\section{Identification and parameter tuning}

\begin{figure}[t]
    \centering
    \includegraphics[width=.75\textwidth]{figs/triangle_models.eps}
    \caption{Steady-state behavior of the LQR controller after four iterations}
    \label{fig:triangle_models}
    \vspace{-1em}
\end{figure}

\subsection{Model identification}

This process is similar to the one described in Section~\ref{sec:ident_tuning}.
We begin with an initial guess of model parameters and iteratively refine them.

In addition, we also need to determine, which model of forces and torques is most suitable.
To do so, we compare the performance of the control systems when stabilizing the triangular object.
This comparison is shown in \figref{fig:triangle_models}.
The control system based on the `Only edges' model defined in \eqref{eq:along_edge} fails to stabilize the object.
The other two control systems manage to stabilize the object, but the control system based on the `Closest point' model defined in \eqref{eq:closest_point} has a slower settling time as well as greater oscillations, especially in the orientation $\theta$.
Therefore, it is reasonable to further consider only the `Full volume' model defined in \eqref{eq:full_volume}.

The results of iterative model identification are shown in \figref{fig:ident_triangle}.
The final values of the model parameters are:
\begin{align}
    d_t &= 0.814\,, & d_r &= 1.16\,, & g_t &= 5.99\,, & g_r &= 0.0243\,.
\end{align}

\begin{figure}[t]
    \centering
    \includegraphics[width=.95\textwidth]{figs/ident_triangle.eps}
    \caption{Results of iterative model identification}
    \label{fig:ident_triangle}
    \vspace{-1em}
\end{figure}

\subsection{Parameter tuning}

The weight matrices $\mat{Q}$ and $\mat{R}$ are tuned similarly as in the previous chapter.
The resulting closed-loop system should be as fast as possible without overshooting, and the required amplitude of pressure should not be saturated (\abbrv{i.e.}, should be less than $1700$ pascals) if the error in position is less than \SI{10}{\milli\metre}.
For orientation, the threshold is $5$ degrees.

The resulting LQR weights are:
\begin{align}
    \mat{Q}_t &= \begin{bmatrix} 1 & 0 \\ 0 & 10 \end{bmatrix}\,, &
    \mat{R}_t &= 40\,, &
    \mat{Q}_r &= \begin{bmatrix} 1 & 0 \\ 0 & 10 \end{bmatrix}\,, &
    \mat{R}_r &= \num{8e-5}\,,
\end{align}
and the resulting MPC weights are:
\begin{align}
    \mat{Q}_t &= \begin{bmatrix} 1 & 0 \\ 0 & 10 \end{bmatrix}\,, &
    \mat{Q}_r &= 100\,\begin{bmatrix} 1 & 0 \\ 0 & 10 \end{bmatrix}\,, &
    \mat{R} &= 1\,.
\end{align}
The control horizon length is five samples, and the prediction horizon length is ten samples.

The noise characteristics for the Kalman filter are also obtained similarly as in the previous chapter.
The covariance of the measured position, $\mat{V}_t$, and the measured orientation, $\mat{V}_r$, is:
\begin{align}
    \mat{V}_t &= 0.873\,, &
    \mat{V}_r &= 0.001\,,
\end{align}
and the covariance matrices of the translational and rotational process noise are:
\begin{align}
    \mat{Q}_t &= \begin{bmatrix} 0.933 & 0.0187\\ 0.0187 & 0.965 \end{bmatrix}\,,&
    \mat{Q}_r &= \begin{bmatrix} 5.09 & 0.102\\ 0.102 & 5.25 \end{bmatrix} \times 10^{-3}\,.
\end{align}
The normalized innovation autocovariance sequences are shown in \figref{fig:xcov_triangle}.
These sequences are considerably higher than the ones of spherical objects shown in \figref{fig:xcov}, which is probably caused by some unmodeled coupling between the translational and rotational dynamics.

\begin{figure}[t]
    \centering
    \includegraphics[width=.8\textwidth]{figs/xcov_triangle.eps}
    \caption{Normalized autocovariance of innovation sequences}
    \label{fig:xcov_triangle}
    \vspace{-1em}
\end{figure}

\section{Results}

To assess the performance of the control systems, I have designed a testing trajectory, where the object rotates and orbits around a given center.
The reference position, $x_{ri}$ and $y_{ri}$, and the reference orientation, $\theta_{ri}$, of the $i^{\rm th}$ object are given by:
\begin{subequations}
\begin{align}
    x_{ri}(t) &= x_{ci} + r_i\,\cos\left( \vartheta_i(t) \right)\,, &
    y_{ri}(t) &= y_{ci} + r_i\,\sin\left( \vartheta_i(t) \right)\,, &
    \vartheta_i(t) &= \frac{2\pi}{T_o}\,\left(t + \frac{i}{n}T_o\right)\,, \\
    \theta_{ri}(t) &= \omega\,\left(t + \frac{i}{n}T_r\right)\,, & & &
    \omega &= \frac{2\pi}{T_r}\,,
\end{align}
\end{subequations}
where $(x_{ci},\,y_{ci})$ are the coordinates of the center of orbit, $r_i$ is the orbital radius, $T_o$ is the orbital period, and $T_r$ is the period of rotation.

The comparison between the LQR and MPC controllers when tracking this `orbit' trajectory with $x_{c} = y_{c} = 0$, $r = 25$, and $T_o = T_r = 30$ is shown in \figref{fig:LQR_MPC}.
Clearly, MPC is more robust, allowing it to track the reference more precisely.
Footage of the MPC with two triangular objects is featured in the supplementary video (see Appendix~\ref{sec:video}).

\begin{figure}[t]
    \centering
    \includegraphics[width=.8\textwidth]{figs/triangle_LQR_MPC.eps}
    \caption{Comparison between LQR and MPC controllers}
    \label{fig:LQR_MPC}
    \vspace{-1em}
\end{figure}