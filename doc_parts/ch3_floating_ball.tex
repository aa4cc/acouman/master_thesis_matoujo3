\chapter[Manipulation of floating spherical objects]{Manipulation of floating\\ spherical objects}
\label{chap:balls}

In this chapter, I describe the mathematical models and control systems involved in manipulating multiple floating spherical objects.
All of the described experiments were done with polypropylene balls having \SI{10}{\milli\metre} in diameter.

\section{Modeling a floating object}

Due to its symmetry, a floating sphere can be modeled as a point mass with coordinates $x$ and $y$.
According to Newton's second law, the dynamics of the object are:
\begin{align}
    m\,\ddot{x} &= - c_d\,\dot{x} + F_x\,, & m\,\ddot{y} &= - c_d\,\dot{y} + F_y\,,
    \label{eq:Newton}
\end{align}
where $m$ is the mass of the object, $c_d$ is the coefficient of drag, and $F_x$ and $F_y$ are the $x$- and $y$-components of the acoustophoretic force, respectively.

When modeling the acoustophoretic force, I used a control-oriented empirical approach.
This means that the created model is simple enough to be computed in real-time and made to reflect the behavior of the system rather than the underlying physics.
Depending on the method of acoustic field shaping, I propose two mathematical models: an amplitude fitting model and a gradient fitting model.
These will be discussed next.

\subsection{`Amplitude fitting' model}

In the case of amplitude fitting, the control strategy is to create a high-pressure point at a fixed distance from the center of the sphere.
The coordinates of this point, $(x_p, y_p)$, can be expressed as:
\begin{align}
    x_p &= x + R\,\cos\alpha\,, & y_p &= y + R\,\sin\alpha\,,
    \label{eq:xpyp}
\end{align}
where $R$ is a fixed distance, and $\alpha$ is a given angle.

Let $P$ be the amplitude of the high-pressure point.
Then, the components of the acoustophoretic force are equal to:
\begin{align}
    F_x &= - c_p \, \max\left(P - 800, 0\right) \cos\alpha\,, & F_y &= - c_p \, \max\left(P - 800, 0\right) \sin\alpha\,,
    \label{eq:amplitude_fitting_force}
\end{align}
where $c_p$ is a conversion coefficient.
The $\max$ function in the formulas implements a dead zone --- the acoustophoretic force is zero until the high-pressure point exceeds $800$ pascals.
This dead zone is caused by the fact that the force depends on the difference of pressures rather than the absolute value, and the amplitude outside the high-pressure point is usually \SI{800}{\pascal}, as can be seen in Figures~\ref{fig:slices_1} and \ref{fig:slices_2}.
The relation between acoustic pressure and force is illustrated in \figref{fig:f2p}.

Let $v_x$ and $v_y$ be velocities of the sphere in $x$- and $y$-coordinate, respectively.
Let us further define inputs $u_x$ and $u_y$ as
\begin{align}
    u_x &= - \max\left(P - 800, 0\right) \cos\alpha\,, & u_y &= - \max\left(P - 800, 0\right) \sin\alpha\,.
    \label{eq:pressure_inputs}
\end{align}
Let us define two sets of states, $\vect{x}_x = \left[v_x, \, x\right]\trans$ and $\vect{x}_y = \left[v_y, \, y\right]\trans$.
Combining the equations above yields the following state-space description of the system:
\begin{subequations}
\begin{align}
    \dot{\vect{x}}_x &= \underbrace{\begin{bmatrix} -\frac{c_d}{m} & 0 \\ 1 & 0 \end{bmatrix}}_{\mat{A}_p}\,\vect{x}_x + \underbrace{\begin{bmatrix} \frac{c_p}{m} \\ 0 \end{bmatrix}}_{\mat{B}_p}\,u_x\,, &
    \dot{\vect{x}}_y &= \mat{A}_p\,\vect{x}_y + \mat{B}_p\,u_y\,, \label{eq:state-space-press} \\
    x &= \underbrace{\begin{bmatrix} 0 & 1 \end{bmatrix}}_{\mat{C}_p}\,\vect{x}_x\,, &    
    y &= \mat{C}_p\,\vect{x}_y\,.
\end{align}
\end{subequations}

\begin{figure}[t]
    \centering
    \begin{subfigure}[t]{0.47 \textwidth}
        \centering
        \includegraphics[width=0.55\textwidth]{figs/f2p.pdf}
        \caption{Acoustophoretic force created by a high-pressure point (represented by orange color).}
        \label{fig:f2p}
    \end{subfigure}
    \begin{subfigure}[t]{0.47 \textwidth}
        \centering
        \includegraphics[width=0.55\textwidth]{figs/f2g.pdf}
        \caption{Acoustophoretic force created by a gradient of acoustic pressure (represented by color gradient in the upper right corner).}
        \label{fig:f2g}
    \end{subfigure}
    \caption{Acoustophoretic forces depending on the shape of the acoustic field}
    \label{fig:acousto_force}
\end{figure}

\subsection{`Gradient fitting' model}

If the acoustic field is shaped by gradient fitting, the point with the specified gradient is equivalent to the center of the sphere.
Let $P_x$ and $P_y$ be the spatial derivatives of acoustic pressure modulus with respect to $x$ and $y$.
Then, the components of the acoustophoretic force are equal to
\begin{align}
    F_x &= - c_g\,P_x\,, & F_y &= - c_g\,P_y\,,
    \label{eq:f2g}
\end{align}
where $c_g$ is a conversion coefficient.
The relation between acoustic pressure gradient and force is illustrated in \figref{fig:f2g}.

Let us use the same sets of states as in the previous section.
Combining \eqref{eq:Newton} and \eqref{eq:f2g} yields the following state space description:
\begin{subequations}
\begin{align}
    \dot{\vect{x}}_x &= \underbrace{\begin{bmatrix} -\frac{c_d}{m} & 0 \\ 1 & 0 \end{bmatrix}}_{\mat{A}_g}\,\vect{x}_x + \underbrace{\begin{bmatrix} -\frac{c_g}{m} \\ 0 \end{bmatrix}}_{\mat{B}_g}\,P_x\,, &
    \dot{\vect{x}}_y &= \mat{A}_g\,\vect{x}_y + \mat{B}_g\,P_y\,, \label{eq:state-space-grad} \\
    x &= \underbrace{\begin{bmatrix} 0 & 1 \end{bmatrix}}_{\mat{C}_g}\,\vect{x}_x\,, &    
    y &= \mat{C}_g\,\vect{x}_y\,.
\end{align}
\end{subequations}

\section{Control system}
\label{sec:control_system}

The control system is created in Simulink.
It runs on the Raspberry Pi with a sampling frequency of \SI{25}{\hertz}.
The system comprises five distinct function blocks: a computer vision block, a bank of predictors (one for each manipulated object), a controller, an optimization algorithm, and a block for communication with the signal generators.
A diagram with signals exchanged between these blocks is shown in \figref{fig:control_system}.

\begin{figure}[b]
    \centering
    \includegraphics[width = \textwidth]{figs/control_system.pdf}
    \caption{Signal diagram of the control system.}
    \label{fig:control_system}
\end{figure}

The computer vision block measures the position of the spheres based on images from the camera.
I use the \emph{raspi-ballpos} script, available at AA4CC research group's github\footnote{\url{https://github.com/aa4cc/raspi-ballpos}}, for this task.
The optimization algorithm is described in Chapter~\ref{chap:optimization}, and the communication script is described in Chapter~\ref{chap:hardware}.
In the following subsections, I will describe the predictor and the controller.

\subsection{Predictor}

Due to the time it takes to process the camera image and the lag in the camera interface, the measured position is delayed by approximately 80 milliseconds, which is equivalent to two control periods.
To compensate for this delay, I have designed a Kalman predictor.
First, the measurement is brought into a Kalman filter, together with the delayed inputs.
The filter outputs delayed estimates of states, $\hat{\vect{x}}_x(t-2)$ and $\hat{\vect{x}}_y(t-2)$.
Undelayed estimates are obtained by applying the discretized state equation:
\begin{subequations}
\begin{align}
    \hat{\vect{x}}_x(t) &= \bar{\vect{A}}^2\,\hat{\vect{x}}_x(t-2) + \bar{\vect{B}}\,u_x(t-1) + \bar{\vect{A}}\,\bar{\vect{B}}\,u_x(t-2) \\
    \hat{\vect{x}}_y(t) &= \bar{\vect{A}}^2\,\hat{\vect{x}}_y(t-2) + \bar{\vect{B}}\,u_y(t-1) + \bar{\vect{A}}\,\bar{\vect{B}}\,u_y(t-2)\,,
\end{align}
\end{subequations}
where $\bar{\vect{A}}$ and $\bar{\vect{B}}$ are matrices obtained by ZOH discretization of the system described in \eqref{eq:state-space-press}, or \eqref{eq:state-space-grad}, depending on the control mode.

\subsection{Controller}

The controller is a relatively complex block and can be further decomposed into function blocks.
The schematic of the controller is shown in \figref{fig:controller}

First, the estimated states are subtracted from reference setpoints and then multiplied by a state feedback gain ${\bf K}$.
The controlled system has a pole in origin.
Therefore, it is possible to achieve zero steady-state error when tracking a constant position reference without augmenting the system with integral control.

Then, the output of the collision avoidance system is added to the result.
The collision avoidance system uses a variant of the commonly used virtual repulsive potential field (first introduced in \cite{collision_avoidance}).
The system outputs control action increments for the $i^{\rm th}$ object, $\Delta u_{xi}$ and $\Delta u_{yi}$, based on the following formulas:
\begin{align}
    \Delta u_{xi} &= c_r\,\nsum[1.5]_{j \in \{1,\ldots,n\}\setminus\{i\}} \frac{\hat{x}_i - \hat{x}_j}{d_{ij}^3}\,, &
    \Delta u_{yi} &= c_r\,\nsum[1.5]_{j \in \{1,\ldots,n\}\setminus\{i\}} \frac{\hat{y}_i - \hat{y}_j}{d_{ij}^3}\,,
\end{align}
where $c_r$ is the repulsive force coefficient and $d_{ij}$ is the mutual distance between $i^{\rm th}$ and $j^{\rm th}$ object.

Let us denote the resulting control actions $\tilde{u}_x$ and $\tilde{u}_y$.
These control actions need to be limited, so they do not exceed the physical capabilities of the acoustic field.
In the case of amplitude fitting, the acoustic pressure must be lower than \SI{2500}{\pascal}.
From \eqref{eq:pressure_inputs}, the relation between the control actions and the required amplitude, $P$, is:
\begin{equation}
    u_x^2 + u_y^2 = \left(P - 800\right)^2\,, \quad \text{if } P \geq 800\,.
    \label{eq:uxuy}
\end{equation}
Therefore, the control actions need to be limited, so that
\begin{equation}
    u_x^2 + u_y^2 \leq 1700^2\,.
    \label{eq:sat_press}
\end{equation}
In the case of gradient fitting, the optimization problem cannot yield a gradient of amplitude modulus greater than \SI{1e9}{\pascal\squared\per\metre} in any direction.
Therefore, the required gradients need to be limited, so that
\begin{equation}
    P_x^2 + P_y^2 \leq \left(10^9\right)^2\,.
    \label{eq:sat_grad}
\end{equation}
In both cases, the constraint has the identical form, which can be generally expressed as $u_x^2 + u_y^2 \leq u_{max}^2\,$.
To satisfy this constraint, I propose a `two-dimensional saturation' function.
Let us define an auxiliary variable $\bar{u} = \sqrt{\bar{u}_x^2 + \bar{u}_y^2}$.
If $\bar{u}$ is less or equal than $u_{max}$, the saturated outputs are equal to the inputs.
However, if $\bar{u}$ is greater than $u_{max}$, the outputs are:
\begin{align}
    u_x &= \frac{u_{max}}{\bar{u}}\,\bar{u}_x\,, &
    u_y &= \frac{u_{max}}{\bar{u}}\,\bar{u}_y\,.
\end{align}

\begin{figure}[t]
    \centering
    \includegraphics[width = .8 \textwidth]{figs/controller.pdf}
    \caption{A signal diagram of the controller block.}
    \label{fig:controller}
    \vspace{-1em}
\end{figure}

In the case of gradient fitting control, the saturated control actions are equal to the required spatial derivatives and the coordinates of the fitted point are equal to the estimated coordinates of the object.
However, when using the amplitude fitting control mode, the saturated control actions need to be converted into position and amplitude of a high-pressure point.
Recalling \eqref{eq:xpyp}, the relations between the control actions and the coordinates of the point are:
\begin{align}
    x_p &= \hat{x} + R\,\cos\alpha\,, & y_p &= \hat{y} + R\,\sin\alpha\,,
\end{align}
where the cosine and sine of the angle $\alpha$ are:
\begin{align}
    \cos\alpha &= - \frac{u_x}{\sqrt{u_x^2 + u_y^2}}\,, &
    \sin\alpha &= - \frac{u_y}{\sqrt{u_x^2 + u_y^2}}\,.
\end{align}
Recalling \eqref{eq:uxuy}, the relation between the control actions and the amplitude is:
\begin{equation}
    P = \sqrt{u_x^2 + u_y^2} + 800\,.
\end{equation}

\section{Identification and parameter tuning}
\label{sec:ident_tuning}

In this section, I explain how are the tasks of model identification and parameter tuning related, what issues does this relation create, and how did I solve them.
To prevent working with too large or too small numbers, I have decided to use SI-prefixed units for some of the quantities and parameters.
Namely, I am using \si{\milli\metre} for coordinates, \si{\milli\metre\per\second} for velocities, \si{\kilo\pascal} for pressure, and \si{\kilo\pascal\squared\per\metre} (equivalent to $10^6$~\si{\pascal\squared\per\metre}) for the gradient of amplitude modulus.

\subsection{Model identification}

Recalling \eqref{eq:state-space-press} and \eqref{eq:state-space-grad}, the matrices $\mat{A}$ and $\mat{B}$ of the state-space description are in a form
\begin{align}
    \mat{A} &= \begin{bmatrix} -d & 0 \\ 1 & 0 \end{bmatrix}\,, &
    \mat{B} &= \begin{bmatrix} g \\ 0 \end{bmatrix}\,,
    \label{eq:ABform}
\end{align}
where $d$ and $g$ are unknown parameters, which need to be identified.

Due to the unstable dynamics of the system, I have decided to use closed-loop identification.
However, this is a kind of chicken-and-egg problem.
We want to identify the model of the system in a closed loop, but to close the loop, we plan to use a model-based controller (LQR+Kalman filter), which we cannot design without the identified model.
While a simple P controller can replace the LQR for the purpose of identification, the Kalman predictor is essential in estimating the object's current position, which is then used to calculate the desired position of the high-pressure point or the gradient.

To solve this problem, I employ an iterative approach.
I start with an initial guess of parameters $d$ and $g$.
Then, I perform an experiment with the closed-loop system and measure the inputs and outputs.
Then, I refine the estimated parameters using the measured data, and the greybox identification tool provided in the Matlab System Identification Toolbox.
This process of measurement and parameter update is repeated until the estimates converge.

Results of the iterative identification are shown in \figref{fig:ident}.
I am using the mean of all experiments as the final value of the parameters.
The values of $d$ slightly differ in amplitude fitting and gradient fitting, with $d_p = 2.48$ and $d_g = 2.20$.
The value of $g_p$ is $134$ and the value of $g_g$ is $-0.122$.

\begin{figure}[t]
    \vspace{-1em}
    \centering
    \begin{subfigure}[b]{0.49 \textwidth}
        \centering
        \includegraphics[width = \textwidth]{figs/ident_press.eps}
        \caption{Identified parameters of amplitude fitting model.}
    \end{subfigure}
    \begin{subfigure}[b]{0.49 \textwidth}
        \centering
        \includegraphics[width = \textwidth]{figs/ident_grad.eps}
        \caption{Identified parameters of gradient fitting model.}
    \end{subfigure}
    \caption{Results of iterative parameter identification.}
    \label{fig:ident}
    \vspace{-1em}
\end{figure}

\subsection{Parameter tuning}

The value of the LQ-optimal state feedback gain $\mat{K}$ is determined by the weight matrices $\mat{Q}$ and $\mat{R}$.
I am using a diagonal matrix for $\mat{Q}$, and since the model of the object is composed of two identical SISO systems, $\mat{R}$ is a scalar.
Therefore, there are three parameters that need to be tuned.

I begin by setting both $\mat{R}$ and the first diagonal element of $\mat{Q}$ to one.
Then, I set the second diagonal element of $\mat{Q}$ so that the simulated response of the system to a step in position reference settles as fast as possible without overshooting.
The resulting matrix $\mat{Q}$ is identical for amplitude fitting and gradient fitting model:
\begin{equation}
    \mat{Q} = \begin{bmatrix} 1 & 0 \\ 0 & 10 \end{bmatrix}\,.
\end{equation}
The value of $\mat{R}$ is then set so that the control action is not saturated if the position error is less than \SI{10}{\milli\metre}.
The resulting value of $\mat{R}$ is \num{1e-8} for amplitude fitting, and \num{1e-5} for gradient fitting.

Another set of iteratively tuned parameters are the noise characteristics for the Kalman filter.
To avoid confusion with the LQR weights, let us denote the process noise covariance matrix as $\mat{W}$ and the measurement noise covariance as $\mat{V}$.
Let us assume that in the continuos-time system, the disturbances only affect the derivative of velocity.
Therefore, the continuos-time stochastic state-space equations of the system are:
\begin{subequations}
\begin{align}
    \dot{\vect{x}} &= \mat{A}\,\vect{x} + \mat{B}\,u + \grvect{\Gamma}\,w\,, \\
    y &= \mat{C}\,\vect{x} + v\,,
\end{align}
\end{subequations}
where $\grvect{\Gamma} = \begin{bmatrix} 1 & 0 \end{bmatrix}^{\rm T}$, and $w$ and $v$ are white, zero-mean uncorrelated Gaussian stochastic processes with covariances $E\left[w^2\right] = q$ and $E\left[v^2\right] = \mat{V}$.
While the covariance of the measurement noise, $\mat{V}$, is identical in the continuous- and discrete-time systems, the covariance of the discrete-time process noise needs to be calculated using the following formula\cite[p.~397]{franklin_digital_1998}:
\begin{equation}
    \mat{W} = \int_{0}^{T_s} {\rm e}^{\mat{A}\tau}\,\grvect{\Gamma}\,q\,\grvect{\Gamma}^{\rm T}\,{\rm e}^{\mat{A}^{\rm T}\tau}\,{\rm d}\tau\,,
\end{equation}
where $T_s$ is the sampling period.

By default, the Matlab implementation of the Kalman filter uses the steady-state Kalman gain, obtained by solving the dual LQR problem \cite{control_system_toolbox}.
Therefore, the performance of the filter is determined by the ratio of the process and measurement noise covariances rather than by their actual values.
If the Kalman filter is optimal, its innovation sequence (\abbrv{i.e.}, the difference between measured and predicted outputs) is uncorrelated \cite{kailath_innovations_1968}.
Therefore, the matrices $\mat{W}$ and $\mat{V}$ can be iteratively tuned until the normalized autocovariance of the resulting innovation sequence is minimal.
The value of $\mat{V}$ can be obtained by calculating the autocovariance of the measured position when the object remains at rest.
The resulting value of $\mat{V}$ is $1.17$.
The matrices $\mat{W}_p$ and $\mat{W}_g$ for amplitude fitting and gradient fitting model, respectively, are:
\begin{align}
    \mat{W}_p &= \left[\begin{array}{cc} 12.0 & 0.12\\ 0.12 & 0.00162 \end{array}\right]\,, &
    \mat{W}_g &= \left[\begin{array}{cc} 24.7 & 0.247\\ 0.247 & 0.00333 \end{array}\right]\,.
\end{align}

The resulting autocovariance sequences are shown in \figref{fig:xcov}.

\begin{figure}[t]
    \vspace{-1em}
    \centering
    \begin{subfigure}[b]{0.49 \textwidth}
        \includegraphics[width = \textwidth]{figs/xcov_ampl.eps}
        \caption{Amplitude fitting model}
        \label{fig:xcov_ampl}
    \end{subfigure}
    \begin{subfigure}[b]{0.49 \textwidth}
        \includegraphics[width = \textwidth]{figs/xcov_grad.eps}
        \caption{Gradient fitting model}
        \label{fig:xcov_grad}
    \end{subfigure}
    \caption{Normalized autocovariance of innovation sequences}
    \label{fig:xcov}
    \vspace{-1em}
\end{figure} 

\section{Results}
\label{sec:ball_results}

To test the ability of the control system to track a moving reference, I have designed two types of reference trajectories.
Let $x_{ri}$ and $y_{ri}$ be the position reference of the $i^{\rm th}$ object.
Then, the first trajectory type can be described by the following equations:
\begin{align}
    x_{ri}(t) &= r_x\,\cos\left( \vartheta_i(t) \right)\,, &
    y_{ri}(t) &= r_y\,\sin\left( 2 \, \vartheta_i(t) \right)\,, &
    \vartheta_i(t) &= \frac{2\pi}{T}\,\left(t + \frac{i}{n}T\right)\,,
\end{align}
where $r_x$ and $r_y$ are the radii in $x$- and $y$-coordinates, $T$ is the period of the trajectory, and $n$ is the total number of objects.
The shape of this trajectory for $r_x = r_y = 40$ and $n=3$ is shown in \figref{fig:traj_inf}.
Note that the objects follow identically shaped trajectories and only differ in starting positions (\abbrv{i.e.,} $x_{ri}(0)$ and $y_{ri}(0)$).
Due to its shape, I am referring to this type as the `Infinity' trajectory.

The second type is inspired by the movement of the end effector of a robotic arm with two links and two free joints.
It is defined by the following equations:
\begin{subequations}
\begin{align}
    x_{ri}(t) &= r_1\,\cos\left( \vartheta_{1i}(t) \right) + r_2\,\cos\left( \vartheta_{2i}(t) \right) \,, &
    \vartheta_{1i}(t) &= \frac{2\pi}{T_1}\,t \,, \\
    y_{ri}(t) &= r_1\,\sin\left( \vartheta_{1i}(t) \right) + r_2\,\sin\left( \vartheta_{2i}(t) \right) \,, &    
    \vartheta_{2i}(t) &= \frac{2\pi}{T_2}\,\left(t + \frac{i}{n}T_2\right) \,,
\end{align}
\end{subequations}
where $r_1$ and $r_2$ are the radii of the links, and $T_1$ and $T_2$ are the periods of the links.
\begin{figure}[hb]
    \centering
    \vspace{0.5em}
    \begin{subfigure}[t]{0.49 \textwidth}
        \includegraphics[width = \textwidth]{figs/trajectory_inf.eps}
        \caption{`Infinity' trajectory for $r_x = r_y = 40$}
        \label{fig:traj_inf}
    \end{subfigure}
    \begin{subfigure}[t]{0.49 \textwidth}
        \includegraphics[width = \textwidth]{figs/trajectory_dlink.eps}
        \caption{`Double link' trajectory for $r_1 = 30$, $r_2 = 20$, and $T_1 = 3\,T_2$}
        \label{fig:traj_dlink}
    \end{subfigure}
    \caption{The tested trajectories}
    \label{fig:trajectories}
    \vspace{-1.5em}
\end{figure} 
The shape of the trajectories for $r_1 = 30$, $r_2 = 20$, $T_1 = 3\,T_2$, and $n=3$ is shown in \figref{fig:traj_dlink}.
Note that the objects follow identically shaped but differently rotated trajectories.
Also, note that at all times, the reference positions form a regular polygon (in this case, an equilateral triangle).
For brevity, I am referring to this type of trajectory as the `Double link' trajectory.

Results of trajectory tracking are shown in \figref{fig:ball_plots}.
Figures~\ref{fig:track_inf} and \ref{fig:track_dlink} show the comparison between amplitude fitting and gradient fitting control when tracking the trajectories.
In both cases, amplitude fitting control is better than gradient fitting control in terms of distance from the reference trajectory.
As shown in \figref{fig:track_20secs}, amplitude fitting control can track the `Infinity' trajectory with a period of 20~seconds, reaching speeds of up to \SI{20}{\milli\metre\per\second} and accelerations of up to \SI{14}{\milli\metre\per\second\squared}, whereas gradient fitting control fails when tracking such a fast-moving reference.
\figref{fig:track_switch} shows how the control system responds to changing the tracked trajectory.
Supplementary video (see Appendix~\ref{sec:video}) then shows how the collision avoidance system prevents the objects from getting too close to each other during the change.

\begin{figure}[h]
    \centering
    \begin{subfigure}[t]{.95 \textwidth}
        \includegraphics[width = \textwidth]{figs/tracking_inf.eps}
        \caption{Performance of amplitude fitting and gradient fitting control when tracking the `Infinity' trajectory}
        \label{fig:track_inf}
    \end{subfigure}
    \begin{subfigure}[t]{.95 \textwidth}
        \includegraphics[width = \textwidth]{figs/tracking_dlink.eps}
        \caption{Performance of amplitude fitting and gradient fitting control when trracking the `Double link' trajectory}
        \label{fig:track_dlink}
    \end{subfigure}
    \vspace{-3em}
\end{figure}
\begin{figure}[h]
    \ContinuedFloat
    \begin{subfigure}[t]{.95 \textwidth}
        \includegraphics[width = \textwidth]{figs/tracking_20secs.eps}
        \caption{Performance of amplitude fitting control when tracking the `Infinity' trajectory with a 20~second period}
        \label{fig:track_20secs}
    \end{subfigure}
    \begin{subfigure}[t]{.95 \textwidth}
        \includegraphics[width = \textwidth]{figs/tracking_switch.eps}
        \caption{Performance of amplitude fitting control when switching between the tracked trajectories}
        \label{fig:track_switch}
    \end{subfigure}
    \caption{Results of trajectory tracking with floating spherical objects}
    \label{fig:ball_plots}
\end{figure}
